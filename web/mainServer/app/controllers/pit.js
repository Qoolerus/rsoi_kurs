const express = require('express');
const router = express.Router();
const PitManager = require('../middleware/pit');
const adapter = require('../frontendAdapt/ymap');
const sockets = require('../sockets/pits');
const fs = require('fs').promises;
const path = require('path');

module.exports = (app) => {
    app.use('/pit', router);
};

router.get('/list', async (req, res) => {
    let skip = req.query['skip'];
    let limit = req.query['limit'];

    let list = await PitManager.getPits(skip, limit, {});

    list.pits = adapter.pitsToYmap(list.pits);

    return res.status(200).send(list);
});

router.put('/add', async (req, res) => {
    res.status(200).send({status: 'OK'});

    let pitInfo = {
        name: req.body.name.toString(),
        longitude: Number(req.body.long),
        latitude: Number(req.body.lat),
        quality: Number(req.body.quality),
        iconSize: Number((req.body.icon_size.toString()).replace(',','.')),
        color: '#'+req.body.color,
        driveId: Number(req.body.drive_id),
        guilt: Number(req.body.guilt.toString().replace(',','.')),
        speed: Number(req.body.speed.toString().replace(',','.')),
        created: Date.now()
    };

    let pit = await PitManager.addPit(pitInfo);

    sockets.sendNewPit(adapter.pitToYmap(pit));
    return;
});

router.get('/drive/:driveId', async (req, res) => {
    let pits = await PitManager.getPitsByDrive(req.params.driveId);

    return res.status(200).send(pits);
});