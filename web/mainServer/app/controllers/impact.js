const express = require('express');
const router = express.Router();
const front = require('./../frontendAdapt/ymap');

module.exports = (app) => {
    app.use('/impact', router);
};

router.get('/', async(req, res) => {
    let drives = front.getTable();

    return res.render('impact', {
        drives
    });
});