const express = require('express');
const router = express.Router();
const sockets = require('../sockets/learning');
const front = require('./../frontendAdapt/ymap');

module.exports = (app) => {
    app.use('/learning', router);
};

router.get('/', async (req, res) => {
    let roads = front.getTable();
    res.render('learningView', {
        roads
    });
    return;
});

router.put('/event', async (req, res) => {
    res.status(200).send({status: 'OK'});
    sockets.sendNewEvent(req.body.re, req.body.ro, req.body.driveid);

    return;
});