const express = require('express');
const router = express.Router();
const front = require('./../frontendAdapt/ymap');

module.exports = (app) => {
    app.use('/', router);
};

router.get('/', async(req, res) => {
    let roads = front.getTable();
    return res.render('viewer', {
        roads
    });
});