const express = require('express');
const router = express.Router();
const pitManager = require('../middleware/pit');
const qualityMapManager = require('../middleware/qualityMap');
const adapter = require('../frontendAdapt/ymap');

module.exports = (app) => {
    app.use('/quality', router);
};

router.get('/', async(req, res) => {
    return res.render('qualityMap');
});

router.get('/tracks', async(req, res) => {

    var pits = await pitManager.getPits(0, Number.MAX_SAFE_INTEGER);
    var nodes = qualityMapManager.getNodesFromPits(pits.pits, 25);
    var tracks = adapter.tracksFromNodesToYmap(nodes);

    return res.status(200).send(tracks);
});

router.get('/spheres', async(req, res) => {

    var pits = await pitManager.getPits(0, Number.MAX_SAFE_INTEGER);
    var nodes = qualityMapManager.getNodesFromPits(pits.pits, 10);
    var spheres = adapter.spheresFromNodesToYmap(nodes, 10);

    return res.status(200).send(spheres);
});