const crypto = require('crypto');
const geoMath = require('../middleware/geoMath');
const uuid = require('uuid/v4');

let colorTable = [];

function trackFromNodeToYmap(node) {
    var nodePoints = node.leafs;
    nodePoints.push(node.root);

    var lineCoords = [];
    var maxDistance = 0;
    for(var i = 0; i < nodePoints.length; i++) {
        for(var j = 0; j < nodePoints.length; j++) {
            var distance = geoMath.getDistanceBetweenPoints({
                latitude: nodePoints[i].latitude,
                longitude: nodePoints[i].longitude
            }, {
                latitude: nodePoints[j].latitude,
                longitude: nodePoints[j].longitude
            });

            if(distance >= maxDistance) {
                maxDistance = distance;
                lineCoords = [
                    [nodePoints[i].latitude, nodePoints[i].longitude],
                    [nodePoints[j].latitude, nodePoints[j].longitude]
                ];
            }
        }
    }

    var track = {
        type: 'Feature',
        id: uuid(),
        options: {
            draggable: false,
            strokeWidth: 6,
            strokeColor: node.meanColor
        },
        geometry: {
            type: 'LineString',
            coordinates: lineCoords,
        }

    }

    return track;
}

function sphereFromNodeToYmap(node) {
    var nodePoints = node.leafs;
    nodePoints.push(node.root);

    var maxDistance = 0;
    for(var i = 0; i < nodePoints.length; i++) {
        for(var j = 0; j < nodePoints.length; j++) {
            var distance = geoMath.getDistanceBetweenPoints({
                latitude: nodePoints[i].latitude,
                longitude: nodePoints[i].longitude
            }, {
                latitude: nodePoints[j].latitude,
                longitude: nodePoints[j].longitude
            });

            if(distance >= maxDistance)
                maxDistance = distance;
        }
    }

    var sphere = {
        type: 'Feature',
        id: uuid(),
        options: {
            draggable: false,
            fillColor: node.meanColor,
            strokeColor: '#000000',
            strokeWidth: 1
        },
        geometry: {
            type: 'Circle',
            coordinates: [node.meanLatitude, node.meanLongitude],
            radius: maxDistance / 2
        }
    }

    return sphere;
}

function pitToYmap(pit) {
    let date = new Date(pit.created);
    let day = date.getDate() <= 9 ? '0'+date.getDate() : date.getDate();
    let month = (date.getMonth() + 1) <= 9 ? '0'+(date.getMonth() + 1) : (date.getMonth() + 1);
    let year = date.getFullYear();
    let hours = date.getHours() <= 9 ? '0'+date.getHours() : date.getHours();
    let minutes = date.getMinutes() <= 9 ? '0'+date.getMinutes() : date.getMinutes();
    let seconds = date.getSeconds() <= 9 ? '0'+date.getSeconds() : date.getSeconds();
    let mills = date.getMilliseconds();

    let impactText = 'Минимальное';
    switch(pit.name) {
        case 'small': impactText = 'Малое'; break;
        case 'medium': impactText = 'Среднее'; break;
        case 'big': impactText = 'Большое'; break;
        case 'very_big': impactText = 'Критическое'; break;
    }
    
    const nPit = {
        type: 'Feature',
        id: pit.id,
        options: {
            draggable: false,
            fillColor: pit.color,
            strokeColor: getColorById(pit.driveId),
            strokeWidth: 2,
        },
        properties: {
            balloonContent: `Влияние на подвеску: ${impactText}<br>
                             Вероятность вины водителя: ${Math.trunc(pit.guilt*100)}%<br>
                             Скорость: ${Math.trunc(pit.speed*3.6*10)/10} км/ч<br>
                             Поездка: ${pit.driveId}<br>
                             Дата: ${day}.${month}.${year} ${hours}:${minutes}:${seconds}.${mills}`,
            hintContent: `Влияние: ${impactText}`
        },
        geometry: {
            type: 'Circle',
            coordinates: pit.mapMatchedLongitude ? [pit.mapMatchedLatitude, pit.mapMatchedLongitude] : [pit.latitude, pit.longitude],
            radius: 1 + Number(pit.iconSize)/10
        }

    }
    return nPit;
};

function tracksFromNodesToYmap(nodes) {
    var adaptedNodes = nodes.map((node) => {
        return trackFromNodeToYmap(node);
    });

    var data = {
        type: "FeatureCollection",
        features: adaptedNodes
    }

    return data;
}

function spheresFromNodesToYmap(nodes, radius) {
    var adaptedNodes = nodes.map((node) => {
        return sphereFromNodeToYmap(node, radius);
    });

    var data = {
        type: "FeatureCollection",
        features: adaptedNodes
    }

    return data;
}

function pitsToYmap(list) {

    list = list.map((item) => {
        return pitToYmap(item);
    });

    let data = {
        type: "FeatureCollection",
        features: list
    };

    return data;
}

function getColorById(id) {
    let record = colorTable.find((item) => {return item.id == id});
    if (record) {
        return record.color;
    } else {
        let item = {
            id: id,
            color: '#' + crypto.randomBytes(3).toString('hex')
        };
        colorTable.push(item);
        return item.color;
    }
}

function getTable() {
    return colorTable.slice();
}

module.exports = {
    pitToYmap,
    pitsToYmap,
    getTable,
    tracksFromNodesToYmap,
    spheresFromNodesToYmap
}