const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PitMapShema = new Schema({
    name: {
        type: String,
        default: ''
    },
    longitude: {
        type: Number,
        required: true,
    },
    latitude: {
        type: Number,
        required: true
    },
    mapMatchedLongitude: {
        type: Number,
        default: 0
    },
    mapMatchedLatitude: {
        type: Number,
        default: 0
    },
    quality: {
        type: Number
    },
    iconSize: {
        type: Number
    },
    color: {
        type: String
    },
    driveId: {
        type: Number
    },
    guilt: {
        type: Number
    },
    speed: {
        type: Number
    },
    created: {
        type: Number
    }
});

//  Добавить точку
PitMapShema.statics.addPit = async function (pitInfo) {
    let doc = new model(pitInfo);
    await doc.save();
    return doc.getRecord();
};


// Обновить точку
PitMapShema.statics.updatePit = async function(filter, data) {
    let doc = await this.findOneAndUpdate(filter, {
        $set: data
    }, {
        new: true,
        runValidators: true
    });

    if(!doc)
        return null;

    return doc.getRecord();
}

//  Получить точки
PitMapShema.statics.getPits = async function (filter, skip = 0, limit = 100, sort = {created: -1}) {
    let docs = await this.find(filter).sort(sort).skip(skip).limit(limit);

    if (docs.length == 0) {
        return [];
    }

    let result = docs.map((item) => {
        return item.getRecord();
    });

    return result;
};

PitMapShema.statics.countPits = async function (filter) {
    let count;
    if (!filter) {
        filter = {};
    }
    // if (filter) {
        count = await this.count(filter);
    // } else {
    //     count = await this.estimatedDocumentCount();
    // }

    return count;
};


PitMapShema.methods.getRecord = function () {
    const data = {
        id: this._id.toString(),
        name: this.name,
        longitude: this.longitude,
        latitude: this.latitude,
        mapMatchedLongitude: this.mapMatchedLongitude,
        mapMatchedLatitude: this.mapMatchedLatitude,
        quality: this.quality,
        iconSize: this.iconSize,
        color: this.color,
        driveId: this.driveId,
        guilt: this.guilt,
        speed: this.speed,
        created: this.created
    };

    return data;
}

const model = mongoose.model('PitMap', PitMapShema);