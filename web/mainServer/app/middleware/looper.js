class Looper {
    constructor(delayTime = 60*60*1000, workFunction = function() {}) {
        this.delayTime = delayTime;
        this.workFunction = workFunction;
        this.intervalId = undefined;
    }

    start() {
        if(this.intervalId)
            this.stop();

        try {
            this.workFunction();
        } catch(err) {
            console.log('Error during looper work: '+err.message);
        };

        this.intervalId = setInterval(() => {
            try {
                this.workFunction();
            } catch(err) {
                console.log('Error during looper work: '+err.message);
            };
        }, this.delayTime);
    }

    stop() {
        clearInterval(this.intervalId);
        this.intervalId = undefined;
    }
}

module.exports = Looper;