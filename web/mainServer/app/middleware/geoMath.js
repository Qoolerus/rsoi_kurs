module.exports = class {
    static getDistanceBetweenPoints(p1, p2) {
        var d2r = Math.PI / 180;
        var equatorialEarthRadius = 6378.1370;
    
        var dlong = (p2.longitude - p1.longitude) * d2r;
        var dlat = (p2.latitude - p1.latitude) * d2r;
        var a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(p1.latitude * d2r) * Math.cos(p2.latitude * d2r) * Math.pow(Math.sin(dlong / 2), 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = equatorialEarthRadius * c;
    
        return d * 1000;
    }

    static getPointProjectionOnLine(pLine1, pLine2, p) {
        let projection = {longitude: 0, latitude: 0};

        let U = ((p.latitude - pLine1.latitude) * (pLine2.latitude - pLine1.latitude)) +
                ((p.longitude - pLine1.longitude) * (pLine2.longitude - pLine1.longitude));

        var UDenom = Math.pow(pLine2.latitude - pLine1.latitude, 2) + Math.pow(pLine2.longitude - pLine1.longitude, 2);

        U /= UDenom;

        projection.latitude = pLine1.latitude + (U * (pLine2.latitude - pLine1.latitude));
        projection.longitude = pLine1.longitude + (U * (pLine2.longitude - pLine1.longitude));

        let minx, maxx, miny, maxy;

        minx = Math.min(pLine1.latitude, pLine2.latitude);
        maxx = Math.max(pLine1.latitude, pLine2.latitude);

        miny = Math.min(pLine1.longitude, pLine2.longitude);
        maxy = Math.max(pLine1.longitude, pLine2.longitude);

        return (projection.latitude >= minx && projection.latitude <= maxx) && (projection.longitude >= miny && projection.longitude <= maxy) ? projection : null;
    }
};