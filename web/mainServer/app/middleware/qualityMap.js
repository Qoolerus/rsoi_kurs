var geoMath = require('./geoMath');

module.exports = class {
    static getNodesFromPits(pits, radius) {
        var nodes = getPitNodes(pits, radius);
        return nodes;
    }
};

function getPitNodes(pits, radius) {
    var nodes = [];
    for(var i = 0; i < pits.length; i++) {
        var node = {
            root: pits[i],
            leafs: [],
            meanQuality: null,
            meanLatitude: null,
            meanLongitude: null,
            meanColor: null
        };

        for(var j = 0; j < pits.length; j++) {
            if(pits[i].id != pits[j].id && geoMath.getDistanceBetweenPoints({
                latitude: pits[i].latitude,
                longitude: pits[i].longitude
            }, {
                latitude: pits[j].latitude,
                longitude: pits[j].longitude
            }) <= radius) {
                node.leafs.push(pits[j]);
            }
        }

        nodes.push(node);
    }

    nodes.sort(function(a, b) {
        if(a.leafs.length > b.leafs.length)
            return -1;
        else
            return 1;
    });

    for(var i = 0; i < nodes.length; i++) {
        if(nodes[i].toDelete)
            continue;

        for(var j = 0; j < nodes[i].leafs.length; j++) {
            var index = nodes.map(function(node) { return node.root.id; }).indexOf(nodes[i].leafs[j].id);
            if(index >= 0) {
                nodes[index].toDelete = true;
                for(var k = 0; k < nodes.length; k++) {
                    if(i == k || nodes[k].toDelete)
                        continue;

                    for(var l = 0; l < nodes[k].leafs.length; l++)
                        if(nodes[k].leafs[l].id == nodes[i].leafs[j].id) {
                            nodes[k].leafs.splice(l, 1);
                            break;
                        }
                }
            }
        }
    }

    for(var i = nodes.length-1; i >= 0; i--)
        if(nodes[i].toDelete)
            nodes.splice(i, 1);

    computeMeanValues(nodes);

    return nodes;
}

function computeMeanValues(nodes) {
    for(var i = 0; i < nodes.length; i++) {
        var meanWeights = 0;

        if(nodes[i].leafs.length) {
            for(var j = 0; j < nodes[i].leafs.length; j++) {
                nodes[i].meanLatitude += nodes[i].leafs[j].latitude;
                nodes[i].meanLongitude += nodes[i].leafs[j].longitude;

                var curWeight = Math.round(nodes[i].leafs[j].created / 1000 / 60 / 60 / 24 * nodes[i].leafs[j].iconSize);
                meanWeights += curWeight;
                nodes[i].meanQuality += nodes[i].leafs[j].quality * curWeight;
            }

            nodes[i].meanLatitude /= nodes[i].leafs.length;
            nodes[i].meanLongitude /= nodes[i].leafs.length;
            nodes[i].meanQuality = Math.round(nodes[i].meanQuality / meanWeights);
            if(nodes[i].meanQuality < 1)
                nodes[i].meanQuality = 1;
            if(nodes[i].meanQuality > 5)
                nodes[i].meanQuality = 5;
        } else {
            nodes[i].meanLatitude = nodes[i].root.latitude;
            nodes[i].meanLongitude = nodes[i].root.longitude;
            nodes[i].meanQuality = nodes[i].root.quality;
        }

        switch(nodes[i].meanQuality) {
            case 1: nodes[i].meanColor = '#ff0000'; break;
            case 2: nodes[i].meanColor = '#f44141'; break;
            case 3: nodes[i].meanColor = '#f4f441'; break;
            case 4: nodes[i].meanColor = '#bbf441'; break;
            case 5: nodes[i].meanColor = '#42f445'; break;
        }
    }
}