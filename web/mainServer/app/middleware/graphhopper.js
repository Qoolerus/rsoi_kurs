const rp = require('request-promise-native');
const uuid = require('uuid/v4');
const fs = require('fs').promises;
const path = require('path');

class GraphhopperAPI {
    constructor(key) {
        this.key = key;
    }

    async getMapMatchingTrek(pits) {
        let gpx = await _pitsToGPXFile(pits);
        let response;

        try {
            response = await rp({
                uri: 'https://graphhopper.com/api/1/match',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/gpx+xml'
                },  
                qs: {
                    key: this.key,
                    vehicle: 'car',
                    optimize: false,
                    instructions: false,
                    points_encoded: false,
                    calc_points: true
                },
                body: await fs.readFile(gpx)
            });

            response = JSON.parse(response);
        } catch(err) {
            await fs.unlink(gpx);
            throw err;
        }
        
        if(!response || !response.paths || !response.paths[0] || !response.paths[0].points || 
           !response.paths[0].points.coordinates || !response.paths[0].points.coordinates.length)
            return null;

        await fs.unlink(gpx);

        let trek = [];
        for(let i = 0; i < response.paths[0].points.coordinates.length; i++)
            trek.push({
                longitude: response.paths[0].points.coordinates[i][0],
                latitude: response.paths[0].points.coordinates[i][1]
            });

        return trek;
    }
}

async function _pitsToGPXFile(pits) {
    if(!pits || !pits.length || pits.length > 500)
        throw new Error('Bad pits count');

    let fileName = uuid();

    await fs.appendFile(path.normalize(__dirname+`/../../${fileName}.gpx`), '<gpx>\n<trk>\n<trkseg>\n');

    for(let i = 0; i < pits.length; i++) {    
        let stringData = `<trkpt lat="${pits[i].latitude}" lon="${pits[i].longitude}"><time>${new Date(pits[i].created).toISOString()}</time></trkpt>\n`;
        await fs.appendFile(path.normalize(__dirname+`/../../${fileName}.gpx`), stringData);
    }
    
    await fs.appendFile(path.normalize(__dirname+`/../../${fileName}.gpx`), '</trkseg>\n</trk>\n</gpx>');

    return path.normalize(__dirname+`/../../${fileName}.gpx`);
}

module.exports = GraphhopperAPI;