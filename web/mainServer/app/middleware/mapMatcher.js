const PitManager = require('./pit');
const GeoMath = require('./geoMath');
const Graphhopper = require('./graphhopper');
const config = require('../../config/config');

const graphhopper = new Graphhopper(config.graphhopper);

class MapMatcher {
    static async mapMatchPits() {
        console.log('Start map matching pits...');

        let drives = await PitManager.getDrivesWithNoMapMatchedPits();

        if(!drives.length)
            return console.log('DONE! All pits are map matched.');

        for(let i = 0; i < drives.length; i++) {
            console.log(`Starting map match drive with id ${drives[i]}...`);

            let page = 0;
            while(true) {
                let pits = await PitManager.getNotMapMatchedPitsByDrive(drives[i], page*500, 500, {created: -1});
                if(!pits || !pits.length) {
                    console.log(`Map matching of drive with id ${drives[i]} is done.`);
                    break;
                }

                console.log(`\tMap matching ${page+1} page...`);
                page++;

                let trek;
                try {
                    trek = await graphhopper.getMapMatchingTrek(pits);
                } catch(err) {}
                
                if(!trek)
                    continue;

                for(let j = 0; j < pits.length; j++) {
                    let closesPointOnTrek = _getPitClosestPointOnTrek(trek, pits[j]);

                    if(closesPointOnTrek && closesPointOnTrek.longitude && closesPointOnTrek.latitude)
                        await PitManager.updatePitById(pits[j].id, {
                            mapMatchedLongitude: closesPointOnTrek.longitude,
                            mapMatchedLatitude: closesPointOnTrek.latitude
                        });
                }
            }
        }

        console.log('DONE! All pits are map matched.');
    }
}

function _getPitClosestPointOnTrek(trek, pit) {
    let closest = {
        point: null,
        distance: null
    };

    closest.point = GeoMath.getPointProjectionOnLine(trek[0], trek[1], pit);
    if(closest.point)
        closest.distance = GeoMath.getDistanceBetweenPoints(pit, closest.point);

    for(let i = 1; i < trek.length-1; i++) {
        let proj = GeoMath.getPointProjectionOnLine(trek[i], trek[i+1], pit);

        if(!proj)
            continue;

        let dist = GeoMath.getDistanceBetweenPoints(pit, proj);

        if(!closest.distance || closest.distance > dist) {
            closest.distance = dist;
            closest.point = proj;
        }
    }

    return closest.point;
}

module.exports = MapMatcher;