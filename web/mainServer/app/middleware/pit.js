const mongoose = require('mongoose');
const model = mongoose.model('PitMap');

class PitMapManager {
    static async addPit(pitInfo) {
        if (!pitInfo || typeof (pitInfo) != 'object') {
            throw new Error('pitInfo is incorrect');
        }

        if (!pitInfo.latitude || !pitInfo.longitude) {
            throw new Error('not found latitude or longitude');
        }

        try {
            return await model.addPit(pitInfo);
        } catch (err) {
            console.log(err.message);
        }
    }

    static async updatePitById(id, data) {
        let updatedPit = await model.updatePit({
            _id: id
        }, data);

        return updatedPit;
    }

    static async getPits(skip, limit, sort = {created: -1}) {
        if (skip === null || skip === undefined) {
            skip = 0;
        }
        skip = Number(skip);

        if (limit === null || limit === undefined) {
            limit = 100;
        }
        limit = Number(limit);

        let actions = [];
        actions.push(model.getPits({}, skip, limit));
        actions.push(model.countPits());
        let [pits, count] = await Promise.all(actions);

        const result = {
            pits:  pits,
            skip: skip,
            limit: limit,
            count: count
        };

        return result;
    }

    static async getPitsByDrive(driveId, skip = 0, limit = 100, sort = {created: -1}) {
        let pits = await model.getPits({
            driveId
        }, skip, limit, sort);

        return pits;
    }

    static async getDrivesWithNoMapMatchedPits() {
        let notMapMatchedPits = await model.getPits({
            mapMatchedLongitude: 0,
            mapMatchedLatitude: 0
        }, 0, Number.MAX_SAFE_INTEGER, {});

        let notMapMatchedDrives = [];
        for(let i = 0; i < notMapMatchedPits.length; i++)
            if(notMapMatchedDrives.indexOf(notMapMatchedPits[i].driveId.toString()) == -1)
                notMapMatchedDrives.push(notMapMatchedPits[i].driveId.toString());

        return notMapMatchedDrives;
    }

    static async getNotMapMatchedPitsByDrive(driveId, skip = 0, limit = 500, sort = {created: -1}) {
        let pits = await model.getPits({
            driveId,
            mapMatchedLongitude: 0,
            mapMatchedLatitude: 0
        }, skip, limit, sort);

        return pits;
    }
}

module.exports = PitMapManager;