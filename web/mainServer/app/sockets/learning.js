const instanceIO = require('./manager');
const uuid = require('uuid/v4');
const fs = require("fs");

var eventList = {};

class learningBus {
    sendNewEvent(re, ro, driveid) {
        var newId = uuid();
        appendEventToList(newId, re, ro);

        var data = {
            id: newId,
            size: re.icon_size,
            skz: Number(getSKZ(re)).toFixed(2),
            name: ro && ro.name ? ro.name : 'unknown',
            color: ro && ro.color ? ro.color : '#ffffff',
            kk: ro && ro.kk ? Number(ro.kk).toFixed(2) : 0,
            driveid: driveid
        };

        instanceIO.socketInstance.sockets.emit('new-event', data);
    }

    bindHandlers(socket) {
        socket.on('classify-event', function(eventId, name) {
            writeEventToFile(eventId, name);
        });
    }
}

function appendEventToList(id, re, ro) {
    eventList[id] = {
        re: re,
        ro: ro
    };

    setTimeout(function() {
        delete eventList[id];
    }, 60000);
}

function writeEventToFile(eventId, name) {
    if(eventList[eventId]) {
        var event = eventList[eventId];
        var quality = '';
        var color = '';
        var size = event.re.icon_size;

        switch(name) {
            case 'very_small':
                quality = '5';
                color = "42f445";
                break;
            case 'small':
                quality = '4';
                color = "bbf441";
                break;
            case 'medium':
                quality = '3';
                color = "f4f441";
                break;
            case 'big':
                quality = '2';
                color = "f44141";
                break;
            case 'very_big':
                quality = '1';
                color = "ff0000";
                break;
        }

        var title = name + '/' + quality + '/' + color + '/' + size + '\n';
        var sensorsData = '';

        for(var i = 0; i < event.re.accelerations.length; i++)
            sensorsData += event.re.accelerations[i][0] + '/' + event.re.accelerations[i][1] + '/' + event.re.accelerations[i][2] + '/' +
                           event.re.positions[i][0] + '/' + event.re.positions[i][1] + '/' + event.re.positions[i][2] + '/' +
                           event.re.velocitys[i][0] + '/' + event.re.velocitys[i][1] + '/' + event.re.velocitys[i][2] + '/' +
                           event.re.gps_speed + '/' + event.re.timecode+'\n';

        sensorsData = sensorsData.split('.').join(',');
        
        fs.appendFile('learning_pits.txt', title+sensorsData+'\n', (err) => {});
    }
}

function getSKZ(re) {
    //finding aZ amplitudes of road event
    var extremums = [];
    var extrFindFlag = false;

    if(re.accelerations.length > 1) {
        for(var i = 1; i < re.accelerations.length; i++) {
            var azCur = Math.abs(re.accelerations[i][2]);
            var azPrev = Math.abs(re.accelerations[i-1][2]);
            
            if(azCur > azPrev) {
                extrFindFlag = false;
            } else {
                if(!extrFindFlag)
                    extremums.push(azPrev);
                extrFindFlag = true;
            }
        }

        if(!extrFindFlag)
            extremums.push(Math.abs(re.accelerations[re.accelerations.length-1][2]));
    } else {
        extremums.push(Math.abs(re.accelerations[0][2]));
    }

    //finding SKZ
    var skz = 0;
    for(var i = 0; i < extremums.length; i++)
        skz += Math.pow(extremums[i], 2);
    skz = Math.sqrt(skz / extremums.length);

    return skz;
}

module.exports = new learningBus();