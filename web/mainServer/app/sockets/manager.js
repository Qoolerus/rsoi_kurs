const io = require('socket.io')(9000);

class SocketPort {
    
    constructor (instance) {
        this._instance = instance;
        this._methods = [];

        this._instance.on('connection', function(socket) {
            require('./learning').bindHandlers(socket);
        });
    }

    get socketInstance () {
        return this._instance;
    }
}

const manager = new SocketPort(io);

module.exports = manager;