const instanceIO = require('./manager');

const bus = new class {
    sendNewPit(pit) {
        instanceIO.socketInstance.sockets.emit('new-pit', pit);
    }
};

module.exports = bus;