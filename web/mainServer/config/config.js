const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'mainserver'
    },
    port: process.env.PORT || 3001,
    db: 'mongodb://localhost/road-dev',
    ymaps: '476c9d8b-17b3-4853-9bd8-9e6c8c74528f',
    graphhopper: '81c0e8dd-40b8-4dd5-849c-6f6e32a9e852'
  },

  test: {
    root: rootPath,
    app: {
      name: 'mainserver'
    },
    port: process.env.PORT || 3001,
    db: 'mongodb://localhost/road-test',
    ymaps: '476c9d8b-17b3-4853-9bd8-9e6c8c74528f',
    graphhopper: '81c0e8dd-40b8-4dd5-849c-6f6e32a9e852'
  },

  production: {
    root: rootPath,
    app: {
      name: 'mainserver'
    },
    port: process.env.PORT || 3001,
    db: 'mongodb://localhost/road-prod',
    ymaps: '476c9d8b-17b3-4853-9bd8-9e6c8c74528f',
    graphhopper: '81c0e8dd-40b8-4dd5-849c-6f6e32a9e852'
  }
};

module.exports = config[env];
