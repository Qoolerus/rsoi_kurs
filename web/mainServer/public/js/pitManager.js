$(document).ready(function () {
    if (!document.getElementById('map'))
        return;

    ymaps.ready(getCurrentPosition);
    connectController.init();
});

function initMap(position) {
    roadMap = new ymaps.Map('map', {
        center: position,
        zoom: 16,
        controls: []
    }, {
            searchControlProvider: 'yandex#search'
        });

    pitManager = new ymaps.ObjectManager();
    roadMap.geoObjects.add(pitManager);
    if (Promise) {
        return new Promise(function (resolve, reject) {
            Pits.render();
            return resolve();
        });
    } else {
        Pits.render();
    }
}

var roadMap;
var pitManager;
var selectedRoadId;

function onView(roadId) {
    var controls = document.querySelectorAll('div#controlls div.list div.elem');
    if (selectedRoadId == roadId) {
        pitManager.objects.each(function (el) {
            el.options.opacity = 1;
        });
        roadMap.geoObjects.removeAll();
        roadMap.geoObjects.add(pitManager);
        for (var i = 0; i < controls.length; i++) {
            controls[i].classList.remove('unselected');
        }
        selectedRoadId = undefined;
        return;
    }
    pitManager.objects.each(function (el) {
        var title = el.properties.balloonContent;
        title = title.split('Поездка: ')[1].split('<br>')[0];
        if (title == roadId) {
            el.options.opacity = 1;
        } else {
            el.options.opacity = 0.1;
        }
    });
    roadMap.geoObjects.removeAll();
    roadMap.geoObjects.add(pitManager);
    var controls = document.querySelectorAll('div#controlls div.list div.elem');
    for (var i = 0; i < controls.length; i++) {
        if (controls[i].id == roadId) {
            controls[i].classList.remove('unselected');
        } else {
            controls[i].classList.add('unselected');
        }
    }
    selectedRoadId = roadId;
}

var Pits = (function () {
    var count = undefined;
    var skip = 0;
    var limit = 1000;

    function getPits(cb) {
        var req = new XMLHttpRequest();
        req.open("GET", '/pit/list?skip=' + skip + '&limit=' + limit, true);
        // req.open("GET", '/pit/list?skip=' + skip + '&limit=' + limit, true);

        req.onreadystatechange = function () {
            if (req.readyState != 4) {
                return;
            }

            if (req.status != 200) {
                return;
            }

            var res = JSON.parse(req.response);
            return cb(res);
        };

        req.send();
    };

    function renderChunk(chunk) {
        pitManager.add(chunk);
    }

    function renderPits() {
        getPits(function (data) {
            if (!count)
                count = data.count;
            renderChunk(data.pits);
            if (skip + data.pits.features.length < count) {
                skip += limit;
                renderPits();
                return;
            } else {

                console.log('Points added');
                return;
            }
        });
    }

    function addPit(pit) {
        pitManager.add(pit);
    }

    return {
        render: renderPits,
        addPit: addPit
    }
})();

function getCurrentPosition() {
    var center = [55.918139, 37.999599];
    return initMap(center);
    // if ('geolocation' in navigator) {
    //     navigator.geolocation.getCurrentPosition(function (position) {
    //         return initMap([position.coords.latitude, position.coords.longitude]);
    //     }, function () {
    //         return initMap(center);
    //     });
    // } else {
    //     return initMap(center);
    // }
}