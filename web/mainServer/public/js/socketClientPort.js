var socket = null;
var connectController = (function () {
    var connectionSettings = {
        ip: '192.168.43.242',
        port: '9000',
        //host: 'http://killaps1.fvds.ru',
        options: {
            path: "/port",
            secure: false
        },

        get addr() {
            if (connectionSettings.host && connectionSettings.host.length > 0)
                return connectionSettings.host;
            return connectionSettings.ip + ':' + connectionSettings.port;
        },

        get opt() {
            if (connectionSettings.host && connectionSettings.host.length > 0) {
                return connectionSettings.options;
            }

            return {};
        }
    }

    function constructor() {
        try {
            socket = io.connect(connectionSettings.addr, connectionSettings.opt);
            bindHandlers();
        } catch (err) {
            console.log('Error to server connect: ' + err.message);
        }
    }

    function bindHandlers() {
        if (socket && socket != undefined) {

            socket.on('new-pit', handleNewPit);

            socket.on('new-event', handleNewEvent);

            socket.on('reconnect', handleTryConnect);

            socket.on('connectionError', function (err) {
                console.log(err);
            });
        } else {
            console.log('fail to set handlers');
        }

        function handleNewPit(point) {
            Pits.addPit(point);
        }

        function handleNewEvent(event) {
            appendNewEvent(event);
        }

        function handleTryConnect() {
            document.location.reload();
        }
    }

    return {
        init: constructor
    };
})();