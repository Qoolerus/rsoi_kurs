function getPits(driveId, cb) {
    var req = new XMLHttpRequest();
    req.open("GET", '/pit/drive/'+driveId, true);

    req.onreadystatechange = function () {
        if (req.readyState != 4)
            return;

        var res = JSON.parse(req.response);
        return cb(res);
    };

    req.send();
}

function changeActiveDrive(driveId) {
    $('.list .elem.selected').removeClass('selected');
    $('.list .elem[drive-id='+driveId+']').addClass('selected');

    getPits(driveId, function(pits) {
        if(!pits || !pits.length)
            return;

        renderPlot(pits);
    });
}

function renderPlot(pits) {
    var impact = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'Impact by time',
        line: {
            width: 1,
        },
        xaxis: {
            type: 'date',
            tickmode: 'linear'
        }
    }

    pits.forEach(pit => {
        var date = new Date(pit.created);
        var hours = date.getHours() <= 9 ? '0' + date.getHours() : date.getHours();
        var minutes = date.getMinutes() <= 9 ? '0' + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() <= 9 ? '0' + date.getSeconds() : date.getSeconds();
        var mseconds = date.getMilliseconds() <= 9 ? '0' + date.getMilliseconds() : date.getMilliseconds();

        //impact.x.push(hours+':'+minutes+':'+seconds+'.'+mseconds);
        impact.x.push(new Date(pit.created).getTime());
        impact.y.push(Math.abs(pit.quality-5)+1);
    });
    
    Plotly.newPlot('plot', [impact]);
}