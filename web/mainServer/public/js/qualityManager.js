var roadMap;
var trackManager;

function initMap(position) {
    roadMap = new ymaps.Map('map', {
        center: position,
        zoom: 16,
        controls: []
    }, {
            searchControlProvider: 'yandex#search'
        });

    trackManager = new ymaps.ObjectManager();
    roadMap.geoObjects.add(trackManager);
    
    getQualityTracks('tracks', function(tracks) {
        renderQualityTracks(tracks);
    });
}

function changeQualityMapType(type) {
    trackManager.removeAll();

    getQualityTracks(type, function(tracks) {
        renderQualityTracks(tracks);
    });
}

function getQualityTracks(type, cb) {
    var req = new XMLHttpRequest();
    req.open("GET", '/quality/'+type, true);
    // req.open("GET", '/quality/'+type, true);

    req.onreadystatechange = function () {
        if (req.readyState != 4) {
            return;
        }

        if (req.status != 200) {
            return;
        }

        var res = JSON.parse(req.response);
        return cb(res);
    };

    req.send();
}

function renderQualityTracks(tracks) {
    trackManager.add(tracks);
}

$(document).ready(function () {
    if (!document.getElementById('map'))
        return;

    ymaps.ready(function() {
        initMap([55.918139, 37.999599]);
    });
});