$(document).ready(function () {
    connectController.init();
});

function appendNewEvent(eventData) {
    if($('select#driveid-select').val() == eventData.driveid+'') {
        $('.road').prepend('<div class="event" event-id="'+eventData.id+'" onclick="updateLearnDialog(&quot;'+eventData.id+'&quot;)"></div><br>');
        $('.event[event-id='+eventData.id+']').css('width', 30 + (Number(eventData.size)*10)+'px');
        $('.event[event-id='+eventData.id+']').css('height', 30 + (Number(eventData.size)*10)+'px');
        $('.event[event-id='+eventData.id+']').css('background', "#" + eventData.color);
        $('.event[event-id='+eventData.id+']').css('box-shadow', '0 0 '+eventData.skz*15+'px '+eventData.skz*5+'px black');
        $('.event[event-id='+eventData.id+']').append('<span class="pit-name">'+eventData.name+'</span>');
        $('.event[event-id='+eventData.id+']').append('<span class="pit-kk">'+eventData.kk+'</span>');
    }
}

function updateLearnDialog(eventId) {
    $('.dialog').attr('selected-event-id', eventId);
    $('.dialog span.pit-name').text($('.event[event-id='+eventId+'] span.pit-name').text());
    $('.dialog span.pit-id').text(eventId);

    $('.event').removeClass('selected');
    $('.event[event-id='+eventId+']').addClass('selected');
}

function classifyButtonClick(name) {
    var eventId = $('.dialog').attr('selected-event-id');
    socket.emit('classify-event', eventId, name);
}