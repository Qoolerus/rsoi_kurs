const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'weatherstatistic'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/weatherstatistic-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'weatherstatistic'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/weatherstatistic-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'weatherstatistic'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/weatherstatistic-production'
  }
};

module.exports = config[env];
