const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const regions = [
	{latitude: 55.9204369, longitude: 37.999161}, // щелково
	{latitude: 55.9529494, longitude: 38.0539833}, // фрязино
	{latitude: 55.8664031, longitude: 37.9892241}, // медвежьи озера
	{latitude: 55.9073911, longitude: 38.0324827}, // чкаловская
	{latitude: 55.9264864, longitude: 37.9493986}, // краснознаменский
	{latitude: 55.9131159, longitude: 37.9195296}, // загорянский
	{latitude: 55.9130197, longitude: 37.8749834} // королев

];

const weather = new Schema({
	latitude: Number,
	longitude: Number,
	days: [{
		year: Number,
		month: Number,
		day: Number,
		hour: Number,
		data: {
			airTemperature: Number,
			//проценты
			humidity: Number,
			//мм рт. ст.
			pressure: Number,
			//проценты
			cloudiness: Number,
			storm: Boolean,
			precipitation: {},
			//м/с
			windSpeed: Number
		}
	}] 
});

//Добавление статистических данных в БД
weather.statics.appendData = function(latitude, longitude, dayData, callback) {
	this.findOne({latitude : latitude, longitude: longitude}, function(err, res) {
		if(err)
			return callback(err, null);

		if(!res) {
			var newDays = [];
			newDays.push({
				year: dayData.year,
				month: dayData.month,
				day: dayData.day,
				hour: dayData.hour,
				data: {
					airTemperature: dayData.airTemperature,
					humidity: dayData.humidity,
					pressure: dayData.pressure,
					cloudiness: dayData.cloudiness,
					storm: dayData.storm,
					precipitation: dayData.precipitation,
					windSpeed: dayData.windSpeed
				}
			});

			var model = mongoose.model('weather');
			var newRegion = new model({
				latitude: latitude,
				longitude: longitude,
				days: newDays
			});
			
			saveRegion(newRegion);
		} else {
			var days = Array.from(res.days);
			days.push({
				year: dayData.year,
				month: dayData.month,
				day: dayData.day,
				hour: dayData.hour,
				data: {
					airTemperature: dayData.airTemperature,
					humidity: dayData.humidity,
					pressure: dayData.pressure,
					cloudiness: dayData.cloudiness,
					storm: dayData.storm,
					precipitation: dayData.precipitation,
					windSpeed: dayData.windSpeed
				}
			});

			res.days = days;
			saveRegion(res);
		}

		function saveRegion(regionModel) {
			regionModel.markModified('days');
			regionModel.save(function(err, saveRes) {
				if(err)
					return callback(err, null);
				if(!saveRes)
					return callback('Cant save data', null);

				callback(null, saveRes.toFullObject());
			});
		}
	});
	
	return;
};

//Получение статистических данных по конкретному дню
weather.statics.getData = function(latitude, longitude, month, day, callback) {
	this.findOne({latitude: latitude, longitude: longitude}, function(err, res) {
		if(err)
			return callback(err, null);
		if(!res)
			return callback('No data for such region', null);

		callback(null, res.getDayData(month, day));
	});
	
	return;
};

//Получение всех доступных регионов
weather.statics.getRegions = function(callback) {
	this.find({}, function(err, res) {
		if(err)
			return callback(err, null);
		if(!res)
			return callback('No data', null);
		if(!res.length)
			return callback(null, []);

		var regions = [];
		for(var i = 0; i < res.length; i++) {
			regions.push({
				latitude: res[i].latitude,
				longitude: res[i].longitude
			});
		}

		callback(null, regions);
	});
	
	return;	
};

//Инициализация регионов
weather.statics.initRegions = function() {
	for(let i = 0; i < regions.length; i++) {
		this.findOne({latitude: regions[i].latitude, longitude: regions[i].longitude}, function(err, res) {
			if(!err && !res) {
				var model = mongoose.model('weather');
				var newRegion = new model({
					latitude: regions[i].latitude,
					longitude: regions[i].longitude,
					days: []
				});

				newRegion.save();
			}
		});
	}

	return;
};

weather.methods.toFullObject = function() {
	var weatherObject = {
		latitude: this.latitude,
		longitude: this.longitude,
		days: this.days
	};

	return weatherObject;
};

weather.methods.getDayData = function(month, day) {
	var data = [];
	for(var i = 0 ; i < this.days.length; i++) {
		if(this.days[i].month == month && this.days[i].day == day)
			data.push(this.days[i]);
	}

	return data;
};

mongoose.model('weather', weather);

