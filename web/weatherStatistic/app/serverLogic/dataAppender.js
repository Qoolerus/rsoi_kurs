var request = require('request');
var weatherMiddle = require('../middleware/weather');
var gismeteoToken = '5abb76f8eadbc3.67178741';

function getCurrentWeatherData(latitude, longitude, callback) {
	request.get({
		headers : {'X-Gismeteo-Token' : gismeteoToken},
		url : 'https://api.gismeteo.ru/v2/weather/current/?latitude='+latitude+'&longitude='+longitude
	}, function(err, gisRes, body) {
		if(gisRes.statusCode == 200)
			callback(JSON.parse(body));
		else
			callback(null);
	});
}

function parseGismeteoData(gisData) {
	var nowDate = new Date();
	let year = nowDate.getFullYear();
	let month = nowDate.getMonth() + 1;
	let day = nowDate.getDate();
	let hour = nowDate.getHours();

	var dbData = {
		airTemperature: gisData.temperature.air.C,
		humidity: gisData.humidity.percent,
		pressure: gisData.pressure.mm_hg_atm,
		cloudiness: gisData.cloudiness.percent,
		storm: gisData.storm,
		precipitation: gisData.precipitation,
		windSpeed: gisData.wind.speed.m_s,
		year: year,
		month: month,
		day: day,
		hour: hour
	};

	return dbData;
}

function updateRegionsData() {
	console.log('Im going to update weather data now!');
	weatherMiddle.getRegions(function(err, regions) {
		if(!err && regions && regions.length) {
			console.log('updating '+regions.length+' regions...');
			for(let i = 0; i < regions.length; i++) {
				setTimeout(() => {
					getCurrentWeatherData(regions[i].latitude, regions[i].longitude, function(data) {
						if(data) {
							var dbData = parseGismeteoData(data.response);
							weatherMiddle.appendData(regions[i].latitude, regions[i].longitude, dbData, function() {});
						}
					});
				}, 3000 * i);
			}
		}
	});
}

//добавлене погодных данных в бд каждые 6 часов, 4 раза в сутки.
setInterval(function() { updateRegionsData(); }, 6 * 60 * 60 * 1000);
setTimeout(function() { updateRegionsData(); }, 5000);
//первоначальная инициализация регионов
weatherMiddle.initRegions();
