var mongoose = require('mongoose');
var weatherManager = mongoose.model('weather');

var weatherStatic = new class weather {
	/**
	 * Добавление новыых данных по конкретному региону
	 * @param {number} latitude широта 
	 * @param {number} longitude долгота
	 * @param {object} dayData объект данных (см. структуру модели)
	 * @param {function} callback 
	 */
	appendData(latitude, longitude, dayData, callback) {
		weatherManager.appendData(latitude, longitude, dayData, callback);
	}

	/**
	 * Получение данных по конкретному дню
	 * @param {number} latitude широта
	 * @param {number} longitude 
	 * @param {number} month 
	 * @param {number} day 
	 * @param {function} callback 
	 */
	getDayData(latitude, longitude, month, day, callback) {
		weatherManager.getData(latitude, longitude, month, day, callback);
	}

	/**
	 * Получить список доступных для просмотра регионов
	 * @param {function} callback 
	 */
	getRegions(callback) {
		weatherManager.getRegions(callback);
	}

	/**
	 * Инициализация регионов
	 */
	initRegions() {
		weatherManager.initRegions();
	}
};

module.exports = weatherStatic;

