var fs = require('fs');

fs.readFile('pits_records.txt', 'utf8', (err, globalSample) => {
    globalSample = globalSample.split('\n\n');

    var testSample = [];
    while(testSample.length < 30) {
        var index = getRandomInt(0, globalSample.length);
        testSample.push(globalSample[index]);
        globalSample.splice(index, 1);
    }

    fs.writeFile('testSample.txt', testSample.join('\n\n'));
    fs.writeFile('globalSample.txt', globalSample.join('\n\n'));
});

function getRandomInt(inclusiveMin, exclusiveMax) {
    min = Math.ceil(inclusiveMin);
    max = Math.floor(exclusiveMax);
    return Math.floor(Math.random() * (max - min)) + min;
}