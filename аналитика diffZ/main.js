function initPlot(data) {
    var lines = data.split('\n');
    lines.splice(75000, lines.length - 75000);

    // chart vars
    var eventStarts = {
        x: [],
        y: [],
        mode: 'markers',
        name: 'Starts',
    };

    var eventStops = {
        x: [],
        y: [],
        mode: 'markers',
        name: 'Stops',
    }

    var diff = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'Diff',
        line: {width: 1}
    }

    var ckoef = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'C',
        line: {width: 1}
    }

    var speedChart = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'Speed',
        line: {width: 1}
    }

    var signalChart = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'signal',
        line: {width: 1}
    }

    var porogChart = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'porog',
        line: {width: 1}
    }

    var diffAndSpeed = {
        x: [],
        y: [],
        mode: 'markers',
        marker: {
            size: 3
        }
    }

    var layout = {
        yaxis: {range: [0, 65]}
    };

    //event searcher vars
    var eventStartFound = false;
    var eventEndFound = false;
    var eventEndFoundCounter = 0;
    var eventTempEndTime = -1;
    var time = 0;

    //processing vars
    var lag = 50;
    var threshold = 5;
    var influence = 0;
    
    var y = [];
    for(var i = 0; i < lines.length; i++) {
        var curLine = lines[i].split('/');
        y.push(Number(curLine[1].replace(',','.')));
    }

    var signals = [];
    for(var i = 0; i < lines.length; i++)
        signals.push(1);

    var filteredY = [];
    for(var i = 0; i < lag; i++) 
        filteredY.push(y[i]);

    var avgFilter = [];
    avgFilter[lag] = mean(y.slice(0, lag));

    var stdFilter = [];
    stdFilter[lag] = std(y.slice(0, lag));

    // main loop
    
    for(var i = lag; i < lines.length; i++) {
        var lineData = lines[i].split('/');

        time = i;// Number(lineData[0].replace(',','.'));
        var diffz = Number(lineData[1].replace(',','.'));
        var speed = Number(lineData[2].replace(',','.'));

        diff.x.push(time);
        diff.y.push(diffz);

        speedChart.x.push(time);
        speedChart.y.push(speed);

        // var koef = 98;
        // var C = 0.02 * koef;
        // if (speed == 0)
        //     C = 0.00155 * koef;
        // else if (speed >= 1 && speed < 10)
        //     C = 0.00425 * koef;
        // else if (speed >= 10 && speed < 20)
        //     C = 0.00575 * koef;
        // else if (speed >= 20 && speed < 30)
        //     C = 0.00675 * koef;
        // else if (speed >= 30 && speed < 40)
        //     C = 0.00775 * koef;
        // else if (speed >= 40 && speed < 50)
        //     C = 0.009 * koef;
        // else if (speed >= 50 && speed < 60)
        //     C = 0.01 * koef;
        // else if (speed >= 60)
        //     C = 0.011 * koef;

        if(speed == 0)
            C = 1;
        else
            C = 0.005977011 * speed + 0.321264395;

        ckoef.x.push(time);
        ckoef.y.push(C);

        if (eventStartFound) {
            if (diffz < C) {
                eventEndFoundCounter++;
                if(eventTempEndTime == -1)
                    eventTempEndTime = time
                if (eventEndFoundCounter >= 24) {
                    eventEndFound = true;
                    eventStops.x.push(eventTempEndTime);
                    eventStops.y.push(1);
                    eventEndFoundCounter = 0;
                    eventTempEndTime = -1;
                }
            } else {
                eventEndFoundCounter = 0;
                eventTempEndTime = -1;
            }
        } else {
            if (diffz > C) {
                eventStartFound = true;
                eventStarts.x.push(time);
                eventStarts.y.push(1);
            }
        }

        if(eventStartFound && eventEndFound) {
            eventEndFound = false;
            eventStartFound = false;
        }

        diffAndSpeed.x.push(speed);
        diffAndSpeed.y.push(diffz);

        if(Math.abs(y[i] - avgFilter[i-1]) > threshold*stdFilter[i-1]) {
            if(y[i] > avgFilter[i-1])
                signals[i]+=0.1;
        
            filteredY[i] = influence*y[i] + (1-influence)*filteredY[i-1];
        } else {
            signals[i] = 1;
            filteredY[i] = y[i];
        }

        avgFilter[i] = mean(filteredY.slice(i-lag, i));
        stdFilter[i] = std(filteredY.slice(i-lag, i));

        signalChart.x.push(time);
        signalChart.y.push(signals[i]);

        porogChart.x.push(time);
        porogChart.y.push(avgFilter[i]+threshold*stdFilter[i]);
    }
    
    //Plotly.newPlot('plot', [eventStarts, eventStops, diff, ckoef, speedChart], layout);
    //Plotly.newPlot('plot', [diff, signalChart, porogChart, ckoef, eventStarts, eventStops]);
    Plotly.newPlot('plot', [diffAndSpeed]);
}

function mean(values) {
    var capaciter = 0;
    for(var i = 0; i < values.length; i++)
        capaciter += values[i];

    return capaciter / values.length;
}

function std(values) {
    var meanVal = mean(values);
    var quadroVals = values.map(function(val) {
        return Math.pow(val - meanVal, 2);
    });

    var quadroSum = 0;
    for(var i = 0; i < quadroVals.length; i++)
        quadroSum += quadroVals[i];

    return Math.sqrt(quadroSum/values.length);
}

window.onload = function () { 
    var fileSelected = document.getElementById('file-input');
    fileSelected.addEventListener('change', function (e) { 
        var fileTobeRead = fileSelected.files[0];
        var fileReader = new FileReader(); 
        fileReader.onload = function (e) {
            var fileContents = document.getElementById('filecontents'); 
            initPlot(fileReader.result); 
        }
        fileReader.readAsText(fileTobeRead); 
    }, false);
} 