function initPlot(data) {
    var zAccelDataChart = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'aZ',
        line: {width: 1}
    }

    var lines = data.split('\n');

    for(var i = 0; i < lines.length; i++) {
        var lineData = lines[i].split('/');
        var az = Math.abs(lineData[2]);

        zAccelDataChart.x.push(i);
        zAccelDataChart.y.push(az);
    }

    //find extremums
    var extremumsChart = {
        x: [],
        y: [],
        mode: 'markers',
        name: 'Amplitudes',
    }

    var extremums = [];
    var extrFindFlag = false;

    if(lines.length > 1) {
        for(var i = 1; i < lines.length; i++) {
            var azCur = Math.abs(lines[i].split('/')[2]);
            var azPrev = Math.abs(lines[i-1].split('/')[2]);
            
            if(azCur > azPrev) {
                extrFindFlag = false;
            } else {
                if(!extrFindFlag)
                    extremums.push({
                        x: i-1,
                        y: azPrev
                    });
                extrFindFlag = true;
            }
        }

        if(!extrFindFlag)
            extremums.push({
                x: lines.length-1,
                y: Math.abs(lines[lines.length-1].split('/')[2])
            });
    } else {
        extremums.push({
            x: 0,
            y: Math.abs(lines[0].split('/')[2])
        });
    }

    for(var i = 0; i < extremums.length; i++) {
        extremumsChart.x.push(extremums[i].x);
        extremumsChart.y.push(extremums[i].y);
    }

    //extremums are amplitudes!
    //find SKZ
    var skzChart = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'SKZ',
        line: {width: 1}
    }

    var skz = 0;
    for(var i = 0; i < extremums.length; i++)
        skz += Math.pow(extremums[i].y, 2);
    skz = Math.sqrt(skz / extremums.length);

    for(var i = 0; i < lines.length; i++) {
        skzChart.x.push(i);
        skzChart.y.push(skz);
    }

    //find average amplitudes
    var averageAmplitudesChart = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'Average Amplitudes',
        line: {width: 1}
    }

    var avgAmps = 0;
    for(var i = 0; i < extremums.length; i++)
        avgAmps += extremums[i].y;
    avgAmps /= extremums.length;

    for(var i = 0; i < lines.length; i++) {
        averageAmplitudesChart.x.push(i);
        averageAmplitudesChart.y.push(avgAmps);
    }

    Plotly.newPlot('plot', [zAccelDataChart, extremumsChart, skzChart, averageAmplitudesChart]);
}

window.onload = function () { 
    var fileSelected = document.getElementById('file-input');
    fileSelected.addEventListener('change', function (e) { 
        var fileTobeRead = fileSelected.files[0];
        var fileReader = new FileReader(); 
        
        fileReader.onload = function (e) {
            var fileContents = document.getElementById('filecontents'); 
            initPlot(fileReader.result); 
        }
        fileReader.readAsText(fileTobeRead); 
    }, false);
} 