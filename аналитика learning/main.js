function initPlot(data) {
    var pits = data.split('\n\n');

    var speedDistribution = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'Speed distribution',
        line: {width: 1}
    }

    var speedDistrArray = [];
    for(var i = 0; i < 80; i++)
        speedDistrArray.push(0);

    for(var i = 0; i < pits.length-1; i++) {
        var pitInfo = pits[i].split('\n');
        var curSpeed = Math.floor(Number(pitInfo[1].split('/')[9].replace(',','.'))*3.6);

        speedDistrArray[curSpeed]++;
    }

    for(var i = 0; i < speedDistrArray.length; i++) {
        speedDistribution.x.push(i);
        speedDistribution.y.push(speedDistrArray[i]);
    }

    var pitTypeDistribution = {
        x: [],
        y: [],
        mode: 'lines',
        name: 'Pit type distribution',
        line: {width: 1}
    }

    var typeArray = [];
    for(var i = 0; i < 5; i++)
        typeArray.push(0);

    for(var i = 0; i < pits.length; i++) {
        var pitInfo = pits[i].split('\n');
        var curType = pitInfo[0].split('/')[0];

        switch(curType) {
            case 'very_small':
                typeArray[0]++;
                break;

            case 'small':
                typeArray[1]++;
                break;

            case 'medium':
                typeArray[2]++;
                break;
            
            case 'big':
                typeArray[3]++;
                break;

            case 'very_big':
                typeArray[4]++;
                break;
        }
    }

    for(var i = 0; i < typeArray.length; i++) {
        pitTypeDistribution.x.push(i);
        pitTypeDistribution.y.push(typeArray[i]);
    }
    
    Plotly.newPlot('plot', [speedDistribution]);
    // Plotly.newPlot('plot', [pitTypeDistribution]);
}

window.onload = function () { 
    var fileSelected = document.getElementById('file-input');
    fileSelected.addEventListener('change', function (e) { 
        var fileTobeRead = fileSelected.files[0];
        var fileReader = new FileReader(); 
        
        fileReader.onload = function (e) {
            var fileContents = document.getElementById('filecontents'); 
            initPlot(fileReader.result); 
        }
        fileReader.readAsText(fileTobeRead); 
    }, false);
} 