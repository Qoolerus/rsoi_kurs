using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Hardware;
using Android.Media;
using MathNet.Numerics.LinearAlgebra;

namespace RoadDataGather
{
    public class Smartphone_Gatherer
    {
        private double NS2S = 1.0f / 1000000000.0f;
        private bool gyro_matrix_init = true;
        private float filter_coef = 0.9f;

        //gyroscope data
        private float[] gyro_speeds = new float[3];
        private float[] gyro_matrix = new float[9];
        private float[] gyro_angles = new float[3];

        //accelerometer/magnetometer data
        private float[] accel = new float[3];
        private float[] magnet = new float[3];
        private float[] acc_mag_matrix = new float[9];
        private float[] acc_mag_angles = new float[3];

        private int accel_smoother;
        private float[] storage_accel = new float[3];
        private float[] smooth_accel = new float[3];

        //alpha-beta filter angles
        private float[] final_angles = new float[3];

        //physical data
        private float[] speed = new float[3];
        private float[] distance = new float[3];
        private float total_speed;
        private float total_distance;

        private SensorManager sens_manager;
        private MediaRecorder mrecorder;
        private object sensor_locker = new object();
        private System.Timers.Timer gather_timer;
        private System.Timers.Timer filter_gyro_matrix_update_timer;
        private System.Timers.Timer filter_final_angles_update_timer;
        private double timestamp;
        private double dt;
        private double timestamp_gather;
        private double dt_gather;

        public double gather_interval { get; set; }


        public event EventHandler<object> Data_updated_event;

        public Smartphone_Gatherer(SensorManager s_m, MediaRecorder m_r, double gather_interval)
        {
            gather_timer = new System.Timers.Timer(gather_interval);
            this.gather_interval = gather_interval;
            gather_timer.AutoReset = true;
            gather_timer.Elapsed += (object sender, System.Timers.ElapsedEventArgs e) =>
            {
                if (timestamp_gather != 0)
                {
                    dt_gather = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).Ticks - timestamp_gather)/TimeSpan.TicksPerSecond;
                    Data_updated_event_handler(GetStatistic());
                }

                timestamp_gather = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).Ticks;
            };

            filter_gyro_matrix_update_timer = new System.Timers.Timer(500);
            filter_gyro_matrix_update_timer.AutoReset = true;
            filter_gyro_matrix_update_timer.Elapsed += Filter_timer_Elapsed;
            //filter_gyro_matrix_update_timer.Start();

            filter_final_angles_update_timer = new System.Timers.Timer(gather_interval);
            filter_final_angles_update_timer.AutoReset = true;
            filter_final_angles_update_timer.Elapsed += Filter_final_angles_update_timer_Elapsed; ;
            filter_final_angles_update_timer.Start();

            sens_manager = s_m;
            mrecorder = m_r;

            gyro_matrix[0] = 1.0f; gyro_matrix[1] = 0.0f; gyro_matrix[2] = 0.0f;
            gyro_matrix[3] = 0.0f; gyro_matrix[4] = 1.0f; gyro_matrix[5] = 0.0f;
            gyro_matrix[6] = 0.0f; gyro_matrix[7] = 0.0f; gyro_matrix[8] = 1.0f;
        }

        public void Place_it_OnSensorChanged(SensorEvent sensor_event)
        {
            lock (sensor_locker)
            {
                if (sensor_event.Sensor.Type == SensorType.Accelerometer)
                {
                    sensor_event.Values.CopyTo(accel, 0);
                    Accel_smooth(this.accel);
                    if (SensorManager.GetRotationMatrix(acc_mag_matrix, null, smooth_accel, magnet))
                        SensorManager.GetOrientation(acc_mag_matrix, acc_mag_angles);
                }

                if (sensor_event.Sensor.Type == SensorType.MagneticField)
                {
                    sensor_event.Values.CopyTo(magnet, 0);
                }

                if (sensor_event.Sensor.Type == SensorType.Gyroscope)
                {
                    if (acc_mag_angles[0] != 0 && acc_mag_angles[1] != 0 && acc_mag_angles[2] != 0)
                    {
                        if (gyro_matrix_init)
                        {
                            float[] init_matrix = new float[9];
                            init_matrix = Get_rotation_matrix_from_angles(acc_mag_angles);
                            gyro_matrix = Matrix_multiplication(gyro_matrix, init_matrix);
                            gyro_matrix_init = false;
                        }

                        float[] delta_vector = new float[4];
                        if (timestamp != 0)
                        {
                            dt = (sensor_event.Timestamp - timestamp) * NS2S;
                            sensor_event.Values.CopyTo(gyro_speeds, 0);
                            Get_quaternion_from_gyro(gyro_speeds, delta_vector, (float)dt);
                        }

                        timestamp = sensor_event.Timestamp;

                        float[] delta_matrix = new float[9];
                        SensorManager.GetRotationMatrixFromVector(delta_matrix, delta_vector);
                        gyro_matrix = Matrix_multiplication(gyro_matrix, delta_matrix);
                        SensorManager.GetOrientation(gyro_matrix, gyro_angles);
                    }
                }
            }
        }

        private void Accel_smooth(float[] accel)
        {
            storage_accel[0] += accel[0];
            storage_accel[1] += accel[1];
            storage_accel[2] += accel[2];

            accel_smoother++;

            if (accel_smoother >= 8)
            {
                smooth_accel[0] = storage_accel[0] / 8;
                smooth_accel[1] = storage_accel[1] / 8;
                smooth_accel[2] = storage_accel[2] / 8;

                storage_accel[0] = 0;
                storage_accel[1] = 0;
                storage_accel[2] = 0;

                accel_smoother = 0;
            }
        }

        private void Get_quaternion_from_gyro(float[] gyro_speeds, float[] delta_rot_vector, float dt)
        {
            float[] norm_gyro_speeds = new float[3];

            float omega_magnitude = (float)Math.Sqrt(gyro_speeds[0] * gyro_speeds[0]
                + gyro_speeds[1] * gyro_speeds[1]
                + gyro_speeds[2] * gyro_speeds[2]);

            if (omega_magnitude > 0.000000001f)
            {
                norm_gyro_speeds[0] = gyro_speeds[0] / omega_magnitude;
                norm_gyro_speeds[1] = gyro_speeds[1] / omega_magnitude;
                norm_gyro_speeds[2] = gyro_speeds[2] / omega_magnitude;
            }

            float theta_over_two = omega_magnitude * dt/2.0f;
            float sin_theta_over_two = (float)Math.Sin(theta_over_two);
            float cos_theta_over_two = (float)Math.Cos(theta_over_two);
            delta_rot_vector[0] = sin_theta_over_two * norm_gyro_speeds[0];
            delta_rot_vector[1] = sin_theta_over_two * norm_gyro_speeds[1];
            delta_rot_vector[2] = sin_theta_over_two * norm_gyro_speeds[2];
            delta_rot_vector[3] = cos_theta_over_two;
        }

        private float[] Matrix_multiplication(float[] A, float[] B)
        {
            float[] result = new float[9];

            result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
            result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
            result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];

            result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
            result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
            result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];

            result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
            result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
            result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];

            return result;
        }

        private float[] Get_rotation_matrix_from_angles(float[] angles)
        {
            float[] xM = new float[9];
            float[] yM = new float[9];
            float[] zM = new float[9];

            float sinX = (float)Math.Sin(angles[1]);
            float cosX = (float)Math.Cos(angles[1]);
            float sinY = (float)Math.Sin(angles[2]);
            float cosY = (float)Math.Cos(angles[2]);
            float sinZ = (float)Math.Sin(angles[0]);
            float cosZ = (float)Math.Cos(angles[0]);

            // rotation about x-axis (pitch)
            xM[0] = 1.0f; xM[1] = 0.0f; xM[2] = 0.0f;
            xM[3] = 0.0f; xM[4] = cosX; xM[5] = sinX;
            xM[6] = 0.0f; xM[7] = -sinX; xM[8] = cosX;

            // rotation about y-axis (roll)
            yM[0] = cosY; yM[1] = 0.0f; yM[2] = sinY;
            yM[3] = 0.0f; yM[4] = 1.0f; yM[5] = 0.0f;
            yM[6] = -sinY; yM[7] = 0.0f; yM[8] = cosY;

            // rotation about z-axis (azimuth)
            zM[0] = cosZ; zM[1] = sinZ; zM[2] = 0.0f;
            zM[3] = -sinZ; zM[4] = cosZ; zM[5] = 0.0f;
            zM[6] = 0.0f; zM[7] = 0.0f; zM[8] = 1.0f;

            // rotation order is y, x, z (roll, pitch, azimuth)
            float[] result_matrix = Matrix_multiplication(xM, yM);
            result_matrix = Matrix_multiplication(zM, result_matrix);
            return result_matrix;
        }


        private void Filter_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (sensor_locker)
            {
                gyro_matrix = Get_rotation_matrix_from_angles(final_angles);
                final_angles.CopyTo(gyro_angles, 0);
            }
        }

        private void Filter_final_angles_update_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (sensor_locker)
            {
                final_angles[0] = filter_coef * gyro_angles[0] + (1 - filter_coef) * acc_mag_angles[0];
                final_angles[1] = filter_coef * gyro_angles[1] + (1 - filter_coef) * acc_mag_angles[1];
                final_angles[2] = filter_coef * gyro_angles[2] + (1 - filter_coef) * acc_mag_angles[2];
            }
        }

        public RoadStatistics GetStatistic()
        {
            //projections
            float[] projections = new float[3];
            projections[0] = smooth_accel[0] * (float)Math.Cos(final_angles[2]) * (float)Math.Cos(final_angles[0]) +
                smooth_accel[1] * (float)Math.Cos(final_angles[1]) * (float)Math.Cos(final_angles[0]) +
                smooth_accel[2] * (float)Math.Sin(final_angles[1]) * (float)Math.Sin(final_angles[2]);

            projections[1] = smooth_accel[0] * (float)Math.Cos(final_angles[2]) * (float)Math.Sin(final_angles[0]) +
                smooth_accel[1] * (float)Math.Sin(final_angles[1]) * (float)Math.Cos(final_angles[0]) +
                smooth_accel[2] * (float)Math.Cos(final_angles[1]) * (float)Math.Cos(final_angles[2]);

            projections[2] = smooth_accel[0] * (float)Math.Sin(final_angles[2]) * (float)Math.Sin(final_angles[0]) +
                smooth_accel[1] * (float)Math.Sin(final_angles[1]) * (float)Math.Sin(final_angles[0]) +
                smooth_accel[2] * (float)Math.Sin(final_angles[1]) * (float)Math.Cos(final_angles[2]);

            speed[0] = projections[0] * (float)dt_gather / 2.0f;
            speed[1] = projections[1] * (float)dt_gather / 2.0f;
            speed[2] = projections[2] * (float)dt_gather / 2.0f;

            total_speed = (float)Math.Sqrt(speed[0] * speed[0] + speed[1] * speed[1] + speed[2] * speed[2]);

            distance[0] = speed[0] * (float)dt_gather / 2.0f;
            distance[1] = speed[1] * (float)dt_gather / 2.0f;
            distance[2] = speed[2] * (float)dt_gather / 2.0f;

            total_distance += (float)Math.Sqrt(distance[0] * distance[0] + distance[1] * distance[1] + distance[2] * distance[2]);

            RoadStatistics statistic = new RoadStatistics(this.gyro_angles, this.acc_mag_angles, this.final_angles,
                this.speed, this.distance, this.total_speed, this.total_distance, this.dt, this.dt_gather);
            return statistic;
        }

        public void Start_gathering()
        {
            gather_timer.Start();
            dt_gather = 0;
            total_distance = 0;
            accel_smoother = 0;
            storage_accel = new float[3];
        }

        public void Stop_gathering()
        {
            gather_timer.Stop();
        }

        private void Data_updated_event_handler(object statistic)
        {
            if (Data_updated_event != null)
                Data_updated_event(this, statistic);
        }
    }

    public class RoadStatistics
    {
        public RoadStatistics()
        {
            this.gangles = new float[3];
            this.amangles = new float[3];
            this.fangles = new float[3];
            this.speed = new float[3];
            this.distance = new float[3];
        }
        public RoadStatistics(float[] gyro_angles, float[] acc_mag_angles, float[] final_angles, float[] speed, float[] distance,
            float total_speed, float total_distance, double _dt, double _dtg)
        {
            this.gangles = new float[3];
            this.amangles = new float[3];
            this.fangles = new float[3];
            this.speed = new float[3];
            this.distance = new float[3];

            gyro_angles.CopyTo(this.gangles,0);
            acc_mag_angles.CopyTo(this.amangles, 0);
            final_angles.CopyTo(this.fangles, 0);
            speed.CopyTo(this.speed, 0);
            distance.CopyTo(this.distance, 0);

            this.tspeed = total_speed;
            this.tdistance = total_distance;
            this.dt = _dt;
            this.dtg = _dtg;
        }

        public float[] gangles { get; set; }
        public float[] amangles { get; set; }
        public float[] fangles { get; set; }
        public float[] speed { get; set; }
        public float[] distance { get; set; }
        public float tspeed { get; set; }
        public float tdistance { get; set; }
        public double dt { get; set; }
        public double dtg { get; set; }
    }
}