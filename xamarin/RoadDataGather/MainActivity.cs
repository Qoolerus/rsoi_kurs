﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Hardware;
using Android.Content;
using Android.Media;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.IO;
using System.Linq;
//using Android.Locations;
using System.Net;
using System.Net.Http;

using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Xamarin.Forms;
using Newtonsoft.Json;
using Android.Gms.Tasks;

namespace RoadDataGather
{
    [Activity(Label = "RoadDataGather", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity, GoogleApiClient.IConnectionCallbacks,
        GoogleApiClient.IOnConnectionFailedListener, ILocationListener
    {
        TextView text;
        TextView bl_text;
        TextView did_text;
        ToggleButton bl_button;
        ToggleButton wd_button;
        BluetoothClient bt;
        ArduinoGatherer a_gatherer;
        RoadEventSearcher event_searcher;
        RoadObjectsClassificator classificator;
        System.Object data_recieved_locker = new System.Object();
        List<string> data_fifo = new List<string>();
        BackgroundWorker worker = new BackgroundWorker();
        List<DataPacket> packets_for_treat = new List<DataPacket>();
        HttpClient http_client = new HttpClient();
        string log_path = Android.OS.Environment.ExternalStorageDirectory.Path+ "/road_scanner/RDG_log.txt";
        string raw_path = Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/RDG_raw.txt";
        string chk_data = Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/Поездка2.txt";
        string pit_records = Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/pits.txt";
        string learning_pits = Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/learning_pits.txt";
        string mac_adress = Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/macs/mac.txt";

        //StreamWriter sw = null;
        StreamWriter sw2 = null;
        StreamWriter sw3 = null;
        StreamReader mac_adress_reader = null;

        //LocationManager locMgr;
        GPSHelper gps_helper = new GPSHelper();
        GoogleApiClient apiClient;

        int event_counter = 0;
        int learn_counter = 0;
        bool learningMode = false;
        bool learningAlertActive = false;
        RoadEvent learningRoadEvent;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            RequestPermissions(new string[] { Android.Manifest.Permission.WriteExternalStorage, Android.Manifest.Permission.Bluetooth, Android.Manifest.Permission.BluetoothAdmin, Android.Manifest.Permission.AccessFineLocation }, 0);

            text = FindViewById<TextView>(Resource.Id.accel_textview);
            bl_text = FindViewById<TextView>(Resource.Id.bluetooth_textview);
            did_text = FindViewById<TextView>(Resource.Id.driveid_textview);
            bl_button = FindViewById<ToggleButton>(Resource.Id.bluetooth_toggle);
            wd_button = FindViewById<ToggleButton>(Resource.Id.writediffs_toggle);

            //bluetooth
            mac_adress_reader = new StreamReader(mac_adress);
            string mac = mac_adress_reader.ReadLine();
            bt = new BluetoothClient(mac, this);//("00:21:13:00:B8:A8", this);
            bt.DataRecieved += Bt_DataRecieved;
            bt.ConnectionFailed += Bt_ConnectionFailed;
            bl_button.CheckedChange += Bl_button_CheckedChange;
            wd_button.CheckedChange += Wd_button_CheckedChange;

            //arduino data gatherer
            a_gatherer = new ArduinoGatherer();

            worker.DoWork += Worker_DoWork;
            //sw = new StreamWriter(log_path, true);
            sw2 = new StreamWriter(raw_path, true);
            //sw3 = new StreamWriter(learning_pits, true);

            event_searcher = new RoadEventSearcher();
            event_searcher.road_event_found += Event_searcher_road_event_found;

            classificator = new RoadObjectsClassificator(this);
            //locMgr = GetSystemService(Context.LocationService) as LocationManager;
            //locMgr.RequestLocationUpdates(LocationManager.GpsProvider, 250, 1, this);

            apiClient = new GoogleApiClient.Builder(this, this, this)
                .AddApi(LocationServices.API).Build();
            apiClient.Connect();

            //event_searcher.EventEmitter();
        }

        async private void Event_searcher_road_event_found(object sender, RoadEvent e)
        {
            event_counter++;

            ThreadStart thrs = new ThreadStart(() =>
            {
                classificator.ClassificateByRandomForest(e, async (object classificator_sender, RoadObject classificated_object) =>
                {
                    try
                    {
                        // Sending add pit request
                        if (classificated_object != null)
                        {
                            var values = new Dictionary<string, string>()
                            {
                                {"name", classificated_object.name },
                                {"long", classificated_object.gps_long.ToString().Replace(',', '.')},
                                {"lat", classificated_object.gps_alt.ToString().Replace(',', '.')},
                                {"quality", classificated_object.quality.ToString()},
                                {"drive_id", bt.connection_id.ToString()},
                                {"guilt", classificated_object.guilt_probability.ToString()},
                                {"icon_size", classificated_object.icon_size.ToString()},
                                {"speed", classificated_object.gps_speed.ToString()},
                                {"color", classificated_object.color.ToString()}
                            };
                            var add_pit_content = new FormUrlEncodedContent(values);

                            int add_pit_retries_count = 10;
                            while(true)
                            {
                                try
                                {
                                    var add_pit_request = new HttpRequestMessage();
                                    add_pit_request.Method = HttpMethod.Put;
                                    add_pit_request.RequestUri = new Uri("http://192.168.43.242:3001/pit/add");
                                    add_pit_request.Content = add_pit_content;

                                    await http_client.SendAsync(add_pit_request);
                                    break;
                                }
                                catch (Exception exc_add_pit)
                                {
                                    await System.Threading.Tasks.Task.Delay(100);
                                    add_pit_retries_count--;

                                    if (add_pit_retries_count == 0)
                                    {
                                        sw2.WriteLine(exc_add_pit.Message.ToString());
                                        break;
                                    }
                                }
                            }
                        }

                        // Sending learning request
                        LearningData data_to_send = new LearningData();
                        data_to_send.driveid = bt.connection_id;
                        data_to_send.re = e;
                        if (classificated_object != null)
                            data_to_send.ro = classificated_object;

                        string json = JsonConvert.SerializeObject(data_to_send);
                        var learning_content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

                        int learning_retries_count = 10;
                        while(true)
                        {
                            try
                            {
                                var learning_request = new HttpRequestMessage();
                                learning_request.Method = HttpMethod.Put;
                                learning_request.RequestUri = new Uri("http://192.168.43.242:3001/learning/event");
                                learning_request.Content = learning_content;

                                await http_client.SendAsync(learning_request);
                                break;
                            }
                            catch (Exception exc_learning)
                            {
                                await System.Threading.Tasks.Task.Delay(100);
                                learning_retries_count--;

                                if (learning_retries_count == 0)
                                {
                                    sw2.WriteLine(exc_learning.Message.ToString());
                                    break;
                                }
                            }
                        }
                    }
                    catch(Exception exc_common)
                    {
                        sw2.WriteLine(exc_common.Message.ToString());
                    }
                });
            });

            Thread thr = new Thread(thrs);
            thr.Start();
        }

        private void learning_pit_type_choosed(object sender, DialogClickEventArgs e)
        {
            string pit_name = "";
            string pit_color = "";
            string pit_quality = "";

            switch(e.Which)
            {
                case 0:
                    {
                        pit_name = "very_small";
                        pit_quality = "5";
                        pit_color = "#42f445";
                        break;
                    }
                case 1:
                    {
                        pit_name = "small";
                        pit_quality = "4";
                        pit_color = "#bbf441";
                        break;
                    }
                case 2:
                    {
                        pit_name = "medium";
                        pit_quality = "3";
                        pit_color = "#f4f441";
                        break;
                    }
                case 3:
                    {
                        pit_name = "big";
                        pit_quality = "2";
                        pit_color = "#f44141";
                        break;
                    }
                case 4:
                    {
                        pit_name = "very_big";
                        pit_quality = "1";
                        pit_color = "#ff0000";
                        break;
                    }
            }

            if (e.Which < 5)
            {
                int pit_size = Convert.ToInt32(learningRoadEvent.duration * 100) > 6 ? 6 : Convert.ToInt32(learningRoadEvent.duration * 100);
                var pit_title = pit_name + "/" + pit_quality + "/" + pit_color + "/" + pit_size;

                sw3.WriteLine(pit_title);

                for (int i = 0; i < learningRoadEvent.accelerations.Count; i++)
                {
                    sw3.WriteLine(learningRoadEvent.accelerations[i][0].ToString() + "/" + learningRoadEvent.accelerations[i][1].ToString() + "/" + learningRoadEvent.accelerations[i][2].ToString() + "/" +
                        learningRoadEvent.positions[i][0].ToString() + "/" + learningRoadEvent.positions[i][1].ToString() + "/" + learningRoadEvent.positions[i][2].ToString() + "/" +
                        learningRoadEvent.velocitys[i][0].ToString() + "/" + learningRoadEvent.velocitys[i][1].ToString() + "/" + learningRoadEvent.velocitys[i][2].ToString() + "/" +
                        learningRoadEvent.gps_speed.ToString() + "/" + learningRoadEvent.timecode.ToString());
                }
                sw3.WriteLine();

                learn_counter++;
            }

            learningAlertActive = false;
        }

        private void Bt_ConnectionFailed(object sender, string e)
        {
            RunOnUiThread(() =>
            {
                if (!e.Contains("socket closed"))
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.SetTitle("Внимание!");
                    alert.SetMessage(e);
                    alert.SetPositiveButton("Ок", (senderAlert, args) => { });

                    Dialog dialog = alert.Create();
                    dialog.Show();
                }

                bl_button.Checked = false;
                sw3.Close();

                //foreach(List<List<string>> pit in event_searcher.list_for_pit_records)
                //{
                //    sw3.WriteLine("\nnew_pit");
                //    foreach (List<string> pit_parts in pit)
                //    {
                //        string pit_rec = "";
                //        foreach (string rec in pit_parts)
                //            pit_rec += rec + "/";
                //        pit_rec = pit_rec.Remove(pit_rec.Count() - 1, 1);
                //        sw3.WriteLine(pit_rec);
                //    }
                //}
            });
        }

        private void Bt_DataRecieved(object sender, string data)
        {
            if(data != null)
            {
                lock(data_recieved_locker)
                {
                    //var subPackets = data.Split('|');

                    //for(var i = 0; i < subPackets.Length; i++)
                    data_fifo.Add(data);
                }
                if (!worker.IsBusy)
                    worker.RunWorkerAsync();
            } 
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while(data_fifo.Count != 0)
            {
                //data split from fifo
                string current_data = "";
                lock (data_recieved_locker)
                {
                    current_data = data_fifo[0];
                }
                RunOnUiThread(() =>
                {
                    text.SetText(current_data, TextView.BufferType.Normal);
                });

                float[] accel_data = new float[3];
                float[] gyro_data = new float[3];
                float[] gravity_data = new float[4];
                float accel_scale = 16384f;
                float gyro_scale = 131.072f;
                int time = 0;
                //float lat = 0, lon = 0, sp = 0;

                string[] splitted_data = current_data.Split('/');
                if (splitted_data.Length == 7)
                {
                    float.TryParse(splitted_data[0].Replace('.', ','), out accel_data[0]);
                    float.TryParse(splitted_data[1].Replace('.', ','), out accel_data[1]);
                    float.TryParse(splitted_data[2].Replace('.', ','), out accel_data[2]);
                    float.TryParse(splitted_data[3].Replace('.', ','), out gyro_data[0]);
                    float.TryParse(splitted_data[4].Replace('.', ','), out gyro_data[1]);
                    float.TryParse(splitted_data[5].Replace('.', ','), out gyro_data[2]);
                    int.TryParse(splitted_data[6], out time);
                    //float.TryParse(splitted_data[7].Replace('.', ','), out lat);
                    //float.TryParse(splitted_data[8].Replace('.', ','), out lon);
                    //float.TryParse(splitted_data[9].Replace('.', ','), out sp);

                    accel_data[0] /= accel_scale;
                    accel_data[1] /= accel_scale;
                    accel_data[2] /= accel_scale;

                    gyro_data[0] /= gyro_scale;
                    gyro_data[1] /= gyro_scale;
                    gyro_data[2] /= gyro_scale;

                    DataPacket new_packet = new DataPacket();
                    new_packet.accel_data = accel_data;
                    new_packet.gyro_data = gyro_data;
                    new_packet.time_sec = time / 1000000f;
                    new_packet.gps_speed = (float)gps_helper.GetGpsSpeed();
                    var coordsWithCorrection = gps_helper.GetPositionWithTimeCorrection(ArduinoGatherer.Total_time + new_packet.time_sec);
                    new_packet.gps_alt = coordsWithCorrection[0];
                    new_packet.gps_long = coordsWithCorrection[1];

                    //record raw data
                    //sw2.WriteLine(accel_data[0].ToString().Replace(',', '.') + "/" +
                    //              accel_data[1].ToString().Replace(',', '.') + "/" + accel_data[2].ToString().Replace(',', '.') + "|" +
                    //              gyro_data[0].ToString().Replace(',', '.') + "/" + gyro_data[1].ToString().Replace(',', '.') + "/" +
                    //              gyro_data[2].ToString().Replace(',', '.') + "|" + (time / 1000f).ToString().Replace(',', '.') + "|" + coordsWithCorrection[0].ToString().Replace(',', '.') +
                    //              "/" + coordsWithCorrection[1].ToString().Replace(',', '.') + "/" + gps_helper.GetGpsSpeed().ToString().Replace(',', '.'));

                    //treatment of raw data
                    TreatmentDataPacket t_p = a_gatherer.packet_treatment(new_packet);
                    event_searcher.Update(t_p);

                    RunOnUiThread(() =>
                    {
                        bl_text.SetText(string.Format("\nax = {0}\nay = {1}\naz = {2}\n\nvx = {3}\nvy = {4} \nvz = {5}\n\nstate = {6}\n\nEvents={7}\nLearn={8}\nSpeed={9}\n\ndiff_Z={10}\ndiff_X={11}",
                            t_p.acceleration[0], t_p.acceleration[1], t_p.acceleration[2], t_p.velocity[0], t_p.velocity[1], t_p.velocity[2],
                            t_p.state, event_counter, learn_counter, (int)(gps_helper.GetGpsSpeed() * 3.6), event_searcher.avgDiffZ, event_searcher.avgDiffX),
                            TextView.BufferType.Normal);
                    });
                }

                lock (data_recieved_locker)
                {
                    data_fifo.RemoveAt(0);
                }
            }
        }

        private void Bl_button_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.SetPriority(100);
            locationRequest.SetFastestInterval(100);
            locationRequest.SetInterval(100);
            LocationServices.FusedLocationApi.RequestLocationUpdates(apiClient, locationRequest, this);

            if (e.IsChecked)
            {
                sw3 = new StreamWriter(learning_pits, true);
                a_gatherer.Reset();
                bt.Connect();
                did_text.SetText(bt.connection_id.ToString(), TextView.BufferType.Normal);
            }
            else
            {
                bt.Disconnect();
                sw3.Close();
            }
        }

        private void Wd_button_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            if (event_searcher.write_diffs == true)
                event_searcher.write_diffs = false;
            else
                event_searcher.write_diffs = true;
        }

        public void OnProviderEnabled(string provider)
        {

        }
        public void OnProviderDisabled(string provider)
        {

        }
        //public void OnStatusChanged(string provider, Availability status, Bundle extras)
        //{
        //}
        public void OnConnected(Bundle connectionHint)
        {

        }
        public void OnConnectionSuspended(int cause)
        {
            throw new System.NotImplementedException();
        }
        public void OnConnectionFailed(Android.Gms.Common.ConnectionResult result)
        {

        }
        public void OnLocationChanged(Android.Locations.Location location)
        {
            this.gps_helper.Update(location.Longitude, location.Latitude);
        }

        protected override void OnResume()
        {
            base.OnResume();
        }

        protected override void OnPause()
        {
            base.OnPause();
        }
    }
}

