using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Diagnostics;

namespace RoadDataGather
{
    public class ArduinoGatherer
    {
        //gyroscrope data
        public float gx
        {
            get { return _gx; }
        }
        private float _gx;

        public float gy
        {
            get { return _gy; }
        }
        private float _gy;

        public float gz
        {
            get { return _gz; }
        }
        private float _gz;

        //accelerometer
        public float ax
        {
            get { return _ax; }
        }
        private float _ax;

        public float ay
        {
            get { return _ay; }
        }
        private float _ay;

        public float az
        {
            get { return _az; }
        }
        private float _az;

        //time
        public int ms_time
        {
            get { return _ms_time; }
        }
        private int _ms_time;

        public static float Total_time = 0;

        //other data
        private System.Object data_locker;
        private float[] distance = new float[3];
        float[] speed = new float[3];
        private int total_time = 0;
        private float total_distance = 0;
        MahonyAHRS AHRS = new MahonyAHRS(1,0);
        bool isStationary;
        bool isPrevStationary;
        int prevStationaryCount = 0;
        int prevNotStationaryCount = 0;
        float[] lastNotStationaryVel = new float[3];

        //calibration of AHRS filter
        private int calibration_period = 0;
        public bool isCalibrated = false;
        float ave_ax = 0;
        float ave_ay = 0;
        float ave_az = 0;
        float ave_time = 0;

        float[] cax = new float[212];
        float[] cay = new float[212];
        float[] caz = new float[212];
        float[] cgx = new float[212];
        float[] cgy = new float[212];
        float[] cgz = new float[212];
        float[] ctime = new float[212];
        int[] st = new int[212];

        float[] last_velocity = new float[3];
        float[] last_pos = new float[3];
        List<double> magnitudes = new List<double>();

        float tot_t = 0;

        public ArduinoGatherer()
        {
            data_locker = new System.Object();
        }

        private int get_state(float ax, float ay, float az)
        {
            //get magnitude
            double magnitude = 0;
            magnitude = Math.Sqrt(Math.Pow(ax, 2) + Math.Pow(ay, 2) + Math.Pow(az, 2));
            magnitudes.Add(magnitude);

            while (magnitudes.Count > 10)
                magnitudes.RemoveAt(0);

            if (magnitudes.Count >= 10)
            {

                //butterform coefs
                double[] b_h = { 0.999970155760487f, -0.999970155760487f }; double[] a_h = { 1f, -0.999940311520975f };
                double[] b_l = { 0.130694269660125f, 0.130694269660125f }; double[] a_l = { 1f, -0.738611460679750f };

                //filtfilt 
                double[] magfilt = Filtfilt.doFiltfilt(b_h.ToList<double>(), a_h.ToList<double>(), magnitudes).ToArray<double>();
                magfilt[0] = Math.Abs(magfilt[0]);

                magfilt = Filtfilt.doFiltfilt(b_l.ToList<double>(), a_l.ToList<double>(), magfilt.ToList<double>()).ToArray<double>();

                int state = magfilt[magfilt.Length-1] < 0.01f ? 1 : 0;

                return state;
            }
            else
                return 1;
        }

        public TreatmentDataPacket packet_treatment(DataPacket data_packet)
        {
            TreatmentDataPacket t_p = new TreatmentDataPacket();

            //set time
            t_p.time = data_packet.time_sec;
            tot_t += data_packet.time_sec;
            Total_time += data_packet.time_sec;
            t_p.total_time = tot_t;

            //set gps data
            t_p.gps_alt = data_packet.gps_alt;
            t_p.gps_long = data_packet.gps_long;
            t_p.gps_speed = data_packet.gps_speed;

            //AHRS reset
            //double reset_magnitude = Math.Sqrt(Math.Pow(data_packet.accel_data[0] * 9.8f, 2) + Math.Pow(data_packet.accel_data[1] * 9.8f, 2) + Math.Pow(data_packet.accel_data[2] * 9.8f - 9.8f, 2));
            //if (reset_magnitude < 0.5f)
            //{
            //    AHRS = new MahonyAHRS(1, 0);
            //    AHRS.Update(0, 0, 0, 0, 0, 0, t_p.time);
            //}

            //get quaternion
            //if (t_p.state == 1)
            //    AHRS.Kp = 0.5f;
            //else
            //    AHRS.Kp = 0;

            //updating quaternion
            //for (var i = 0; i < 12500; i++)
            //{
            //    AHRS.Update((float)Math.PI / 8, 0, 0, 0, 0, 0, 0.002f);
            //}

            //AHRS.Update(Deg2Rad(data_packet.gyro_data[0]), Deg2Rad(data_packet.gyro_data[1]), Deg2Rad(data_packet.gyro_data[2]),
            //    data_packet.accel_data[0], data_packet.accel_data[1], data_packet.accel_data[2], t_p.time);

            //for (var i = 0; i < 5; i++)
            //    AHRS.MadgwickQuaternionUpdate(data_packet.accel_data[0], data_packet.accel_data[1], data_packet.accel_data[2],
            //        Deg2Rad(data_packet.gyro_data[0]), Deg2Rad(data_packet.gyro_data[1]), Deg2Rad(data_packet.gyro_data[2]), t_p.time);

            //t_p.quat[0] = AHRS.Quaternion[0];
            //t_p.quat[1] = AHRS.Quaternion[1];
            //t_p.quat[2] = AHRS.Quaternion[2];
            //t_p.quat[3] = AHRS.Quaternion[3];

            //getting gravity vector
            //var gravity = new float[3];
            //gravity[0] = 2 * (t_p.quat[1] * t_p.quat[3] - t_p.quat[0] * t_p.quat[2]);
            //gravity[1] = 2 * (t_p.quat[0] * t_p.quat[1] + t_p.quat[2] * t_p.quat[3]);
            //gravity[2] = t_p.quat[0] * t_p.quat[0] - t_p.quat[1] * t_p.quat[1] - t_p.quat[2] * t_p.quat[2] + t_p.quat[3] * t_p.quat[3];

            //getting accels without gravity vector
            //t_p.acceleration[0] = (data_packet.accel_data[0] - data_packet.gravity_data[0]) * 9.8f;
            //t_p.acceleration[1] = (data_packet.accel_data[1] - data_packet.gravity_data[1]) * 9.8f;
            //t_p.acceleration[2] = (data_packet.accel_data[2] - data_packet.gravity_data[2]) * 9.8f;

            //get acceleration without gravity vector
            //float[] rotated_to_earth_accel = new float[3];
            //float[] current_quaternion = new float[4];

            //current_quaternion[0] = t_p.quat[0];
            //current_quaternion[1] = t_p.quat[1];
            //current_quaternion[2] = t_p.quat[2];
            //current_quaternion[3] = t_p.quat[3];

            //rotated_to_earth_accel = AccelRotate(data_packet.accel_data, current_quaternion);

            //rotated_to_earth_accel[0] *= 9.8f;
            //rotated_to_earth_accel[1] *= 9.8f;
            //rotated_to_earth_accel[2] *= 9.8f;
            //rotated_to_earth_accel[2] -= 9.8f;

            //t_p.acceleration[0] = rotated_to_earth_accel[0];
            //t_p.acceleration[1] = rotated_to_earth_accel[1];
            //t_p.acceleration[2] = rotated_to_earth_accel[2];

            t_p.acceleration[0] = data_packet.accel_data[0] * 9.8f;
            t_p.acceleration[1] = data_packet.accel_data[1] * 9.8f;
            t_p.acceleration[2] = data_packet.accel_data[2] * 9.8f;

            t_p.acceleration[0] = Math.Abs(t_p.acceleration[0]) < 0.01 ? 0 : t_p.acceleration[0];
            t_p.acceleration[1] = Math.Abs(t_p.acceleration[1]) < 0.01 ? 0 : t_p.acceleration[1];
            t_p.acceleration[2] = Math.Abs(t_p.acceleration[2]) < 0.01 ? 0 : t_p.acceleration[2];

            //get magnitude
            double magnitude = Math.Sqrt(Math.Pow(t_p.acceleration[0], 2) + Math.Pow(t_p.acceleration[1], 2) + Math.Pow(t_p.acceleration[2], 2));
            t_p.magnitude = (float)magnitude;

            //get state
            t_p.state = t_p.magnitude <= 0.05 ? 1 : 0;

            // get velocity
            if (t_p.state == 0)
            {
                t_p.velocity[0] = last_velocity[0] + (t_p.acceleration[0] * t_p.time);
                t_p.velocity[1] = last_velocity[1] + (t_p.acceleration[1] * t_p.time);
                t_p.velocity[2] = last_velocity[2] + (t_p.acceleration[2] * t_p.time);

                last_velocity[0] = t_p.velocity[0];
                last_velocity[1] = t_p.velocity[1];
                last_velocity[2] = t_p.velocity[2];
            }
            else
            {
                t_p.velocity[0] = 0;
                t_p.velocity[1] = 0;
                t_p.velocity[2] = 0;

                last_velocity[0] = 0;
                last_velocity[1] = 0;
                last_velocity[2] = 0;
            }

            //get position
            t_p.position[0] = last_pos[0] + (t_p.velocity[0] * t_p.time);
            t_p.position[1] = last_pos[1] + (t_p.velocity[1] * t_p.time);
            t_p.position[2] = last_pos[2] + (t_p.velocity[2] * t_p.time);

            last_pos[0] = t_p.position[0];
            last_pos[1] = t_p.position[1];
            last_pos[2] = t_p.position[2];

            return t_p;
        }

         public void calibrate(DataPacket packet)
         {
            if (calibration_period <= 211)
            {
                ave_ax += packet.accel_data[0];
                ave_ay += packet.accel_data[1];
                ave_az += packet.accel_data[2];
                ave_time += packet.time_sec;

                cax[calibration_period] = packet.accel_data[0];
                cay[calibration_period] = packet.accel_data[1];
                caz[calibration_period] = packet.accel_data[2];
                cgx[calibration_period] = packet.gyro_data[0];
                cgy[calibration_period] = packet.gyro_data[1];
                cgz[calibration_period] = packet.gyro_data[2];
                ctime[calibration_period] = packet.time_sec;

                if (calibration_period == 211)
                {
                    ave_ax /= 212;
                    ave_ay /= 212;
                    ave_az /= 212;
                    ave_time /= 212;

                    for (int i = 0; i < 2000; i++)
                    {
                        AHRS.Update(0, 0, 0, ave_ax, ave_ay, ave_az, 0.0095f);
                    }
                    isCalibrated = true;
                }
                calibration_period++;
            }
        }

        public void Reset()
        {
            this.total_distance = 0;
            this.total_time = 0;
            this.isCalibrated = false;
            this.calibration_period = 0;
            this.AHRS = new MahonyAHRS(1, 0);
            this.ave_ax = 0;
            this.ave_ay = 0;
            this.ave_az = 0;
            this.ave_time = 0;
            speed = new float[3];
            prevStationaryCount = 0;
            prevNotStationaryCount = 0;
            lastNotStationaryVel = new float[3];
            last_pos = new float[3];
            last_velocity = new float[3];
            magnitudes.Clear();
            tot_t = 0;
            Total_time = 0;
        }

        private float[] QuaternionGetProduct(float[] q, float[] p)
        {
            float[] r = new float[4];
            r[0] = q[0] * p[0] - q[1] * p[1] - q[2] * p[2] - q[3] * p[3];
            r[1] = q[0] * p[1] + q[1] * p[0] + q[2] * p[3] - q[3] * p[2];
            r[2] = q[0] * p[2] - q[1] * p[3] + q[2] * p[0] + q[3] * p[1];
            r[3] = q[0] * p[3] + q[1] * p[2] - q[2] * p[1] + q[3] * p[0];

            return r;
        }

        private float[] QuaternionGetConjugate(float[] q)
        {
            float[] r = new float[4];
            r[0] = q[0];
            r[1] = -q[1];
            r[2] = -q[2];
            r[3] = -q[3];

            return r;
        }

        private float[] AccelRotate(float[] accel, float[] q)
        {
            float[] accel_rot = new float[3];

            float[] p = new float[4];
            p[0] = 0; p[1] = accel[0]; p[2] = accel[1]; p[3] = accel[2];

            p = QuaternionGetProduct(QuaternionGetProduct(q, p), QuaternionGetConjugate(q));

            accel_rot[0] = p[1];
            accel_rot[1] = p[2];
            accel_rot[2] = p[3];

            return accel_rot;
        }

        private float Deg2Rad(float degrees)
        {
            return (float)(Math.PI / 180) * degrees;
        }

        public ArduinoRoadStatistic GetStatistic()
        {
            float[] acc = new float[3];
            float[] gyr = new float[3];
            float[] dynamic_distance = new float[3];
            float total_speed = 0;
            long last_time = 0;

            float[] quaternion = null;
            float[] a_w_g = null;

            lock (data_locker)
            {
                quaternion = AHRS.Quaternion;
                acc[0] = _ax;
                acc[1] = _ay;
                acc[2] = _az;
                gyr[0] = gx;
                gyr[1] = gy;
                gyr[2] = gz;

                a_w_g = new float[3];
                a_w_g[0] = this._ax; a_w_g[1] = this._ay; a_w_g[2] = this._az;

                a_w_g = AccelRotate(a_w_g, QuaternionGetConjugate(quaternion));

                a_w_g[0] *= 9.81f;
                a_w_g[1] *= 9.81f;
                a_w_g[2] *= 9.81f;

                a_w_g[2] -= 9.81f;

                //get velocity
                float velx = a_w_g[0] * (float)_ms_time / 1000.0f;
                float vely = a_w_g[1] * (float)_ms_time / 1000.0f;
                float velz = a_w_g[2] * (float)_ms_time / 1000.0f;

                //if (Math.Abs(velx) < 0.001f) velx = 0;
                //if (Math.Abs(vely) < 0.001f) vely = 0;
                //if (Math.Abs(velz) < 0.001f) velz = 0;

                speed[0] += velx;
                speed[1] += vely;
                speed[2] += velz;

                //remove integral drift
                if (this.isStationary)
                {
                    if (!isPrevStationary)
                        prevStationaryCount = 0;
                    prevStationaryCount++;
                }
                else
                {
                    if(isPrevStationary)
                        prevNotStationaryCount = 0;

                    prevNotStationaryCount++;

                    lastNotStationaryVel[0] = speed[0];
                    lastNotStationaryVel[1] = speed[1];
                    lastNotStationaryVel[2] = speed[2];

                    float[] drift_rate = new float[3];
                    //drift_rate[0] = lastStationaryVel[0] / prevStationaryCount;
                    //drift_rate[1] = lastStationaryVel[1] / prevStationaryCount;
                    //drift_rate[2] = lastStationaryVel[2] / prevStationaryCount;

                    float[] drift = new float[3];
                    drift[0] = drift_rate[0] * prevNotStationaryCount;
                    drift[1] = drift_rate[1] * prevNotStationaryCount;
                    drift[2] = drift_rate[2] * prevNotStationaryCount;
                }

                //total speed from all 3 axis
                total_speed = (float)Math.Sqrt(speed[0] * speed[0] + speed[1] * speed[1] + speed[2] * speed[2]);

                //get distance
                dynamic_distance[0] = speed[0] * (float)_ms_time / 1000.0f;
                dynamic_distance[1] = speed[1] * (float)_ms_time / 1000.0f;
                dynamic_distance[2] = speed[2] * (float)_ms_time / 1000.0f;

                distance[0] += dynamic_distance[0];
                distance[1] += dynamic_distance[1];
                distance[2] += dynamic_distance[2];

                total_distance += (float)Math.Sqrt(dynamic_distance[0] * dynamic_distance[0] + dynamic_distance[1] * dynamic_distance[1] 
                    + dynamic_distance[2] * dynamic_distance[2]);

                last_time = _ms_time;
                isPrevStationary = isStationary;
            }

            return new ArduinoRoadStatistic(acc, gyr, speed, distance, total_speed, total_distance, last_time, total_time);
        }
    }

    public class DataPacket
    {
        public float[] accel_data { get; set; }
        public float[] gyro_data { get; set; }
        public float time_sec { get; set; }
        public float gps_speed { get; set; }
        public float gps_alt { get; set; }
        public float gps_long { get; set; }

        public DataPacket()
        {
            init();
        }

        public DataPacket(float[] a, float[] g, float t_sec)
        {
            init();
            accel_data = a;
            gyro_data = g;
            time_sec = t_sec;
        }

        private void init()
        {
            accel_data = new float[3];
            gyro_data = new float[3];
            gps_alt = 0;
            gps_long = 0;
            gps_speed = 0;
        }
    }

    public class TreatmentDataPacket
    {
        public float[] acceleration { get; set; }
        public float[] velocity { get; set; }
        public float[] position { get; set; }
        public float[] quat { get; set; }
        public int state { get; set; }
        public float magnitude { get; set; }
        public float time { get; set; }
        public float gps_speed { get; set; }
        public float gps_alt { get; set; }
        public float gps_long { get; set; }
        public float total_time { get; set; }

        public TreatmentDataPacket()
        {
            acceleration = new float[3];
            velocity = new float[3];
            position = new float[3];
            quat = new float[4];
            state = 0;
            time = 0;
            magnitude = 0;
            gps_alt = 0;
            gps_long = 0;
            gps_speed = 0;
            total_time = 0;
        }
    }

    public class ArduinoRoadStatistic
    {
        public float[] accel { get; set; }
        public float[] gyro { get; set; }
        public float[] speed { get; set; }
        public float[] distance { get; set; }
        public float tspeed { get; set; }
        public float tdistance { get; set; }
        public long last_time { get; set; }
        public int ttime { get; set; }

        public ArduinoRoadStatistic(float[] acc, float[] gyr, float[] sp, float[] dis, float tsp, float tdis, long time, int ttime)
        {
            this.speed = new float[3];
            this.distance = new float[3];
            this.accel = new float[3];
            this.gyro = new float[3];

            acc.CopyTo(this.accel, 0);
            gyr.CopyTo(this.gyro, 0);
            this.tspeed = tsp;
            this.tdistance = tdis;
            sp.CopyTo(this.speed, 0);
            dis.CopyTo(this.distance, 0);
            this.last_time = time;
            this.ttime = ttime;
        }
    }
}