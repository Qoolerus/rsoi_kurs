﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RoadDataGather
{
    public class Filtfilt
    {
        public static List<double> doFiltfilt(List<double> B, List<double> A, List<double> X)
        {

            int len = X.Count;
            int na = A.Count;
            int nb = B.Count;
            int nfilt = (nb > na) ? nb : na;
            int nfact = 3 * (nfilt - 1);
            if (len <= nfact)
            {
                throw new Exception("error");
            }
            resize(B, nfilt, 0);
            resize(A, nfilt, 0);

            List<int> rows = new List<int>();
            List<int> cols = new List<int>();

            add_index_range(rows, 0, nfilt - 2, 1);
            if (nfilt > 2)
            {
                add_index_range(rows, 1, nfilt - 2, 1);
                add_index_range(rows, 0, nfilt - 3, 1);
            }
            add_index_const(cols, 0, nfilt - 1);
            if (nfilt > 2)
            {
                add_index_range(cols, 1, nfilt - 2, 1);
                add_index_range(cols, 1, nfilt - 2, 1);
            }
            int klen = rows.Count;
            List<double> data = new List<double>();
            resize(data, klen, 0);
            data[0] = 1 + A[1];
            int j = 1;
            if (nfilt > 2)
            {
                for (int i = 2; i < nfilt; i++)
                {
                    data[j++] = A[i];
                }
                for (int i = 0; i < nfilt - 2; i++)
                {
                    data[j++] = 1.0;
                }
                for (int i = 0; i < nfilt - 2; i++)
                {
                    data[j++] = -1.0;
                }
            }
            List<double> leftpad = subvector_reverse(X, nfact, 1);
            changeArray2(leftpad, 2 * X[0]);

            List<double> rightpad = subvector_reverse(X, len - 2, len - nfact - 1);
            changeArray2(rightpad, 2 * X[len - 1]);

            double y0;
            List<double> signal1 = new List<double>();
            List<double> signal2 = new List<double>();
            List<double> zi = new List<double>();
            //reserve(signal1,leftpad.size() + X.size() + rightpad.size(),0);
            append_vector(signal1, leftpad);
            append_vector(signal1, X);
            append_vector(signal1, rightpad);

            double[][] sp = Zeros(max_val(rows) + 1, max_val(cols) + 1);
            for (int k = 0; k < klen; ++k)
            {
                sp[rows[k]][cols[k]] = data[k];
            }
            double[] bb = map(B);
            double[] aa = map(A);
            //Mat.inv(sp)
            //bb.segment(1, nfilt - 1) - (bb(0) * aa.segment(1, nfilt - 1))

            double[][] ZZi = multi(inv(sp), calc(segment(bb, 1, nfilt - 1), bb[0], segment(aa, 1, nfilt - 1)));

            resize(zi, ZZi.Length, 1);

            changeZi(ZZi, zi, signal1[0]);
            filter(B, A, signal1, signal2, zi);
            reverse(signal2);
            changeZi(ZZi, zi, signal2[0]);
            filter(B, A, signal2, signal1, zi);
            List<double> Y = subvector_reverse(signal1, signal1.Count - nfact - 1, nfact);
            return Y;

        }

        private static void reverse(List<double> signal2)
        {
            int i = 0;
            int j = signal2.Count - 1;
            while (i < j)
            {
                swap(signal2, i, j);
                i++;
                j--;
            }
        }

        private static void swap(List<double> signal2, int i, int j)
        {
            double temp = signal2[j];
            signal2[j] = signal2[i];
            signal2[i] = temp;
        }

        private static void changeZi(double[][] zZi, List<double> zi, double double1)
        {
            for (int i = 0; i < zZi.Length; i++)
            {
                zi[i] = zZi[i][0] * double1;
            }

        }

        private static double[][] calc(double[] segment, double d, double[] segment2)
        {
            //JAVA TO C# CONVERTER NOTE: The following call to the 'RectangularArrays' helper class reproduces the rectangular array initialization that is automatic in Java:
            //ORIGINAL LINE: double[][] ret = new double[segment.Length][1];
            double[][] ret = ReturnRectangularDoubleArray(segment.Length, 1);
            for (int i = 0; i < segment.Length; i++)
            {
                ret[i][0] = segment[i] - d * segment2[i];
            }
            return ret;
        }

        private static double[] segment(double[] bb, int i, int j)
        {
            double[] ret = new double[j - i + 1];
            for (int k = 0; k < j - i + 1; k++)
            {
                ret[k] = bb[i + k];
            }
            return ret;
        }

        private static double[] map(List<double> b)
        {
            double[] ret = new double[b.Count];
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = b[i];
            }
            return ret;
        }

        private static double[][] Zeros(int ii, int jj)
        {
            //JAVA TO C# CONVERTER NOTE: The following call to the 'RectangularArrays' helper class reproduces the rectangular array initialization that is automatic in Java:
            //ORIGINAL LINE: double[][] sp = new double[ii][jj];
            double[][] sp = ReturnRectangularDoubleArray(ii, jj);
            for (int i = 0; i < ii; i++)
            {
                for (int j = 0; j < jj; j++)
                {
                    sp[i][j] = 0;
                }
            }
            return sp;
        }

        public static void filter(List<double> B, List<double> A, List<double> X, List<double> Y, List<double> Zi)
        {
            if (A.Count == 0)
            {
                throw new Exception("error");
            }
            bool flagA = true;
            foreach (double doubleA in A)
            {
                if (doubleA != 0)
                {
                    flagA = false;
                }
            }
            if (flagA)
            {
                throw new Exception("error");
            }
            if (A[0] == 0)
            {
                throw new Exception("error");
            }
            changeArray(A, A[0]);
            changeArray(B, A[0]);


            int input_size = X.Count;
            int filter_order = max(A.Count, B.Count);
            resize(B, filter_order, 0);
            resize(A, filter_order, 0);
            resize(Zi, filter_order, 0);
            resize(Y, input_size, 0);

            for (int i = 0; i < input_size; i++)
            {
                int order = filter_order - 1;
                while (order != 0)
                {
                    if (i >= order)
                    {
                        Zi[order - 1] = B[order] * X[i - order] - A[order] * Y[i - order] + Zi[order];
                    }
                    --order;
                }
                Y[i] = B[0] * X[i] + Zi[0];
            }
            Zi.Remove(Zi.Count - 1);
        }



        private static void resize(List<double> a, int i, double j)
        {
            if (a.Count >= i)
            {
                return;
            }
            int size = a.Count;
            for (int j2 = size; j2 < i; j2++)
            {
                a.Add(j);
            }
        }

        private static int max(int size, int size2)
        {
            if (size > size2)
            {
                return size;
            }
            else
            {
                return size2;
            }
        }

        public static double[][] multi(double[][] a, double[][] b)
        {
            int hang = a.Length;
            int lie = b[0].Length;
            double sum;
            //JAVA TO C# CONVERTER NOTE: The following call to the 'RectangularArrays' helper class reproduces the rectangular array initialization that is automatic in Java:
            //ORIGINAL LINE: double[][] result = new double[hang][lie];
            double[][] result = ReturnRectangularDoubleArray(hang, lie);
            for (int i = 0; i < hang; i++)
            {
                for (int j = 0; j < lie; j++)
                {
                    sum = 0;
                    for (int k = 0; k < b.Length; k++)
                    {
                        sum += a[i][k] * b[k][j];
                    }
                    result[i][j] = sum;
                }
            }
            return result;
        }

        public static double[][] inv(double[][] matrix)
        {
            int n = matrix.Length;
            //JAVA TO C# CONVERTER NOTE: The following call to the 'RectangularArrays' helper class reproduces the rectangular array initialization that is automatic in Java:
            //ORIGINAL LINE: double[][] matrix1 = new double[n][2*n];
            double[][] matrix1 = ReturnRectangularDoubleArray(n, 2 * n);
            //JAVA TO C# CONVERTER NOTE: The following call to the 'RectangularArrays' helper class reproduces the rectangular array initialization that is automatic in Java:
            //ORIGINAL LINE: double[][] result = new double[n][n];
            double[][] result = ReturnRectangularDoubleArray(n, n);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matrix1[i][j] = matrix[i][j];
                }
            }
            for (int k = 0; k < n; k++)
            {
                for (int t = n; t < n * 2; t++)
                {
                    if ((t - k) == n)
                    {
                        matrix1[k][t] = 1.0;
                    }
                    else
                    {
                        matrix1[k][t] = 0;
                    }
                }
            }
            //    		get the change of the array
            for (int k = 0; k < n; k++)
            {
                if (matrix1[k][k] != 1)
                {
                    double bs = matrix1[k][k];
                    matrix1[k][k] = 1;
                    for (int p = k; p < n * 2; p++)
                    {
                        matrix1[k][p] /= bs;
                    }
                }
                for (int q = 0; q < n; q++)
                {
                    if (q != k)
                    {
                        double bs = matrix1[q][k];
                        for (int p = 0; p < n * 2; p++)
                        {
                            matrix1[q][p] -= bs * matrix1[k][p];
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            for (int x = 0; x < n; x++)
            {

                for (int y = n; y < n * 2; y++)
                {
                    result[x][y - n] = matrix1[x][y];
                }
            }


            return result;
        }

        internal static void changeArray(List<double> vec, double a0)
        {
            for (int i = 0; i < vec.Count; i++)
            {
                vec[i] = vec[i] / a0;
            }
        }

        internal static void changeArray2(List<double> vec, double a0)
        {
            for (int i = 0; i < vec.Count; i++)
            {
                vec[i] = a0 - vec[i];
            }
        }

        internal static void add_index_range(List<int> indices, int beg, int end, int inc)
        {
            for (int i = beg; i <= end; i += inc)
            {
                indices.Add(i);
            }
        }

        internal static void add_index_const(List<int> indices, int value, int numel)
        {
            while (numel-- != 0)
            {
                indices.Add(value);
            }
        }

        internal static void append_vector(List<double> vec, List<double> tail)
        {
            foreach (double doubleitem in tail)
            {
                vec.Add(doubleitem);
            }
        }

        internal static List<double> subvector_reverse(List<double> vec, int idx_end, int idx_start)
        {
            List<double> resultArrayList = new List<double>(idx_end - idx_start + 1);
            for (int i = 0; i < idx_end - idx_start + 1; i++)
            {
                resultArrayList.Add(0.0);
            }
            int endindex = idx_end - idx_start;
            for (int i = idx_start; i <= idx_end; i++)
            {
                resultArrayList[endindex--] = vec[i];
            }
            return resultArrayList;
        }

        internal static int max_val(List<int> vec)
        {
            int temp = vec[0];
            foreach (int integer in vec)
            {
                if (temp < integer)
                {
                    temp = integer;
                }
            }
            return temp;
        }

        internal static double[][] ReturnRectangularDoubleArray(int size1, int size2)
        {
            double[][] newArray = new double[size1][];
            for (int array1 = 0; array1 < size1; array1++)
            {
                newArray[array1] = new double[size2];
            }

            return newArray;
        }
    }
}