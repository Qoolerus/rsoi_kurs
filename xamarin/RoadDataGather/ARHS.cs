using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RoadDataGather
{
    public class MahonyAHRS
    {
        /// <summary>
        /// Gets or sets the sample period.
        /// </summary>
        public float SamplePeriod { get; set; }

        /// <summary>
        /// Gets or sets the algorithm proportional gain.
        /// </summary>
        public float Kp { get; set; }

        /// <summary>
        /// Gets or sets the algorithm integral gain.
        /// </summary>
        public float Ki { get; set; }

        /// <summary>
        /// Gets or sets the Quaternion output.
        /// </summary>
        public float[] Quaternion { get; set; }

        /// <summary>
        /// Gets or sets the integral error.
        /// </summary>
        private float[] eInt { get; set; }

        public MahonyAHRS(float kp, float ki)
        {
            Kp = kp;
            Ki = ki;
            Quaternion = new float[] { 1f, 0f, 0f, 0f };
            eInt = new float[] { 0f, 0f, 0f };
        }

        public void MadgwickQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float deltat)
        {
            var Beta = 0f;
            float q1 = Quaternion[0], q2 = Quaternion[1], q3 = Quaternion[2], q4 = Quaternion[3];   // short name local variable for readability
            float norm;
            float s1, s2, s3, s4;
            float qDot1, qDot2, qDot3, qDot4;

            // Auxiliary variables to avoid repeated arithmetic
            float _2q1 = 2f * q1;
            float _2q2 = 2f * q2;
            float _2q3 = 2f * q3;
            float _2q4 = 2f * q4;
            float _4q1 = 4f * q1;
            float _4q2 = 4f * q2;
            float _4q3 = 4f * q3;
            float _8q2 = 8f * q2;
            float _8q3 = 8f * q3;
            float q1q1 = q1 * q1;
            float q2q2 = q2 * q2;
            float q3q3 = q3 * q3;
            float q4q4 = q4 * q4;

            // Normalise accelerometer measurement
            norm = (float)Math.Sqrt(ax * ax + ay * ay + az * az);
            if (norm == 0f) return; // handle NaN
            Beta = norm;
            norm = 1f / norm;        // use reciprocal for division
            ax *= norm;
            ay *= norm;
            az *= norm;

            // Gradient decent algorithm corrective step
            s1 = _4q1 * q3q3 + _2q3 * ax + _4q1 * q2q2 - _2q2 * ay;
            s2 = _4q2 * q4q4 - _2q4 * ax + 4f * q1q1 * q2 - _2q1 * ay - _4q2 + _8q2 * q2q2 + _8q2 * q3q3 + _4q2 * az;
            s3 = 4f * q1q1 * q3 + _2q1 * ax + _4q3 * q4q4 - _2q4 * ay - _4q3 + _8q3 * q2q2 + _8q3 * q3q3 + _4q3 * az;
            s4 = 4f * q2q2 * q4 - _2q2 * ax + 4f * q3q3 * q4 - _2q3 * ay;
            norm = (float)Math.Sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
            if (norm == 0f) return; // handle NaN
            norm = 1f / norm;
            s1 *= norm;
            s2 *= norm;
            s3 *= norm;
            s4 *= norm;

            // Compute rate of change of quaternion
            qDot1 = 0.5f * (-q2 * gx - q3 * gy - q4 * gz) - Beta * s1;
            qDot2 = 0.5f * (q1 * gx + q3 * gz - q4 * gy) - Beta * s2;
            qDot3 = 0.5f * (q1 * gy - q2 * gz + q4 * gx) - Beta * s3;
            qDot4 = 0.5f * (q1 * gz + q2 * gy - q3 * gx) - Beta * s4;

            // Integrate to yield quaternion
            q1 += qDot1 * deltat;
            q2 += qDot2 * deltat;
            q3 += qDot3 * deltat;
            q4 += qDot4 * deltat;
            norm = (float)Math.Sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion
            if (norm == 0f) return; // handle NaN
            norm = 1f / norm;
            Quaternion[0] = q1 * norm;
            Quaternion[1] = q2 * norm;
            Quaternion[2] = q3 * norm;
            Quaternion[3] = q4 * norm;
        }

        public void Update(float gx, float gy, float gz, float ax, float ay, float az, float time)
        {
            float q1 = Quaternion[0], q2 = Quaternion[1], q3 = Quaternion[2], q4 = Quaternion[3];   // short name local variable for readability
            float norm;
            //float vx, vy, vz;
            //float ex, ey, ez;
            //float pa, pb, pc;

            // Normalise accelerometer measurement
            //norm = (float)Math.Sqrt(ax * ax + ay * ay + az * az);
            //if (norm == 0f) return; // handle NaN
            //norm = 1 / norm;        // use reciprocal for division
            //ax *= norm;
            //ay *= norm;
            //az *= norm;

            // Estimated direction of gravity
            //vx = 2.0f * (q2 * q4 - q1 * q3);
            //vy = 2.0f * (q1 * q2 + q3 * q4);
            //vz = q1 * q1 - q2 * q2 - q3 * q3 + q4 * q4;

            // Error is cross product between estimated direction and measured direction of gravity
            //ex = (ay * vz - az * vy);
            //ey = (az * vx - ax * vz);
            //ez = (ax * vy - ay * vx);
            //if (Ki > 0f)
            //{
            //    eInt[0] += ex;      // accumulate integral error
            //    eInt[1] += ey;
            //    eInt[2] += ez;
            //}
            //else
            //{
            //    eInt[0] = 0.0f;     // prevent integral wind up
            //    eInt[1] = 0.0f;
            //    eInt[2] = 0.0f;
            //}

            // Apply feedback terms
            //gx = gx + Kp * ex + Ki * eInt[0];
            //gy = gy + Kp * ey + Ki * eInt[1];
            //gz = gz + Kp * ez + Ki * eInt[2];

            // Integrate rate of change of quaternion
            //pa = q2;
            //pb = q3;
            //pc = q4;
            //q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * time);
            //q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * time);
            //q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * time);
            //q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * time);

            //Integrating quaternion
            var q1_old = q1 * 0.5f;
            var q2_old = q2 * 0.5f;
            var q3_old = q3 * 0.5f;
            var q4_old = q4 * 0.5f;

            q1 = q1_old * 0 - q2_old * gx - q3_old * gy - q4_old * gz;
            q2 = q1_old * gx + q2_old * 0 + q3_old * gz - q4_old * gy;
            q3 = q1_old * gy - q2_old * gz + q3_old * 0 + q4_old * gx;
            q4 = q1_old * gz + q2_old * gy - q3_old * gx + q4_old * 0;

            Quaternion[0] += (q1 * time);
            Quaternion[1] += (q2 * time);
            Quaternion[2] += (q3 * time);
            Quaternion[3] += (q4 * time);

            // Normalise quaternion
            norm = (float)Math.Sqrt(Math.Pow(Quaternion[0], 2) + Math.Pow(Quaternion[1], 2) + Math.Pow(Quaternion[2], 2) + Math.Pow(Quaternion[3], 2));
            Quaternion[0] = Quaternion[0] / norm;
            Quaternion[1] = Quaternion[1] / norm;
            Quaternion[2] = Quaternion[2] / norm;
            Quaternion[3] = Quaternion[3] / norm;
        }
    }
}