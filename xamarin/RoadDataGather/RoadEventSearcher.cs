using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using System.Threading.Tasks;

namespace RoadDataGather
{
    public class RoadEventSearcher
    {
        public List<TreatmentDataPacket> packets { get; set; }
        public float C { get; set; }
        public float avgDiffZ { get; set; }
        public float avgDiffX { get; set; }
        public int T { get; set; }
        public float A { get; set; }

        public event EventHandler<RoadEvent> road_event_found;
        public bool write_diffs { get; set; }

        private List<float[]> g { get; set; }
        private System.Object packets_locker = new System.Object();
        private bool event_start_found = false;
        private bool event_end_found = false;
        private int event_end_found_counter = 0;
        private int i_start = 16;
        private int i_stop = 0;
        private StreamWriter diffWriter;

        public List<List<List<string>>> list_for_pit_records = new List<List<List<string>>>();

        private void road_event_found_handler(RoadEvent founded_event)
        {
            road_event_found?.Invoke(this, founded_event);
        }

        public RoadEventSearcher()
        {
            packets = new List<TreatmentDataPacket>();
            g = new List<float[]>();
            float[] first_g = new float[3];
            first_g[0] = 0; first_g[1] = 0; first_g[2] = 0;
            avgDiffZ = -1;
            avgDiffX = -1;
            for (int i = 0; i < 16; i++)
                g.Add(first_g);
            C = 3f;
            T = 50;
            A = 0.95f;

            diffWriter = new StreamWriter(Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/diffs.txt", true);
            write_diffs = false;
        }

        public async void EventEmitter()
        {
            await Task.Run(async () =>
            {
                Random random = new Random();
                
                await Task.Delay(Convert.ToInt32(random.NextDouble() * (400 - 25) + 25));

                RoadEvent emitted_event = new RoadEvent();
                emitted_event.gps_alt = 12.44f;
                emitted_event.gps_long = 12.44f;
                emitted_event.gps_speed = 12.44f;
                emitted_event.timecode = 10;
                emitted_event.time_sec.Add(10);
                emitted_event.duration = 10;
                emitted_event.icon_size = 10;

                emitted_event.accelerations.Add(new float[3] { -0.2177246f, 0.5479004f, -0.664538562f });
                emitted_event.positions.Add(new float[3] { 115.100906f, -141.706314f, 28.6401253f });
                emitted_event.velocitys.Add(new float[3] { -0.0248598047f, 0.4982277f, -0.259274721f });
                emitted_event.positions_difference.Add(emitted_event.positions[0]);
                emitted_event.velocitys_difference.Add(emitted_event.velocitys[0]);

                road_event_found_handler(emitted_event);

                EventEmitter();
            });
        }

        public void Update(TreatmentDataPacket packet)
        {
            lock (packets_locker)
            {
                this.packets.Add(packet);
            }

            if (packets.Count > 32)
                lock (packets_locker)
                {
                    search();
                }
        }

        private void search()
        {
            var cur_pcount = packets.Count;

            g.Clear();
            for (int i = 0; i < cur_pcount; i++)
                g.Add(new float[] { 0, 0, 0 });

            for(int i = i_start; i < cur_pcount - 16; i++)
            {
                float[] ac_mid = new float[3];
                for (int j = i - 16; j < i + 16; j++)
                {
                    ac_mid[0] += packets[j].acceleration[0];
                    ac_mid[1] += packets[j].acceleration[1];
                    ac_mid[2] += packets[j].acceleration[2];
                }
                ac_mid[0] /= 32;
                ac_mid[1] /= 32;
                ac_mid[2] /= 32;

                g[i][0] = ac_mid[0];
                g[i][1] = ac_mid[1];
                g[i][2] = ac_mid[2];
            }

            for (int i = i_start; i < cur_pcount - 16; i++)
            {
                float diff_x = Math.Abs(packets[i].acceleration[0] - g[i][0]);
                float diff_y = Math.Abs(packets[i].acceleration[1] - g[i][1]);
                float diff_z = Math.Abs(packets[i].acceleration[2] - g[i][2]);

                if (avgDiffX == -1)
                    avgDiffX = diff_x;
                else
                    avgDiffX = (avgDiffX + diff_x) / 2;
                if (avgDiffZ == -1)
                    avgDiffZ = diff_z;
                else
                    avgDiffZ = (avgDiffZ + diff_z) / 2;

                float speed = packets[i].gps_speed * 3.6f;

                if (speed == 0)
                    C = 1;
                else
                    C = 0.00666666f * speed + 0.35666666f;

                if(write_diffs == true)
                {
                    // time/diffz/speed
                    diffWriter.WriteLine(packets[i].time.ToString() + "/" + diff_z.ToString() + "/" + speed.ToString());
                }

                //C = avgDiffZ * 1.9f; // 190% from avgDiffZ to cut noise
                //float speed = packets[i].gps_speed * 3.6f;
                //if (speed == 0)
                //    C = 1;

                if (ArduinoGatherer.Total_time > 8)
                { 
                    if (event_start_found)
                    {
                        if (diff_z < C)
                        {
                            event_end_found_counter++; // waiting silence during 24 packets to mark event as ENDED

                            if (event_end_found_counter >= 24)
                            {
                                event_end_found_counter = 0;
                                event_end_found = true;
                                i_stop = i-24;

                                break;
                            }
                        }
                        else
                            event_end_found_counter = 0;
                    }
                    else
                    {
                        if (diff_z > C)
                        {
                            event_start_found = true;
                            i_start = i;
                        }
                    }
                }
            }
             
            if(event_end_found && event_start_found)
            {
                //record test
                //list_for_pit_records.Add(new List<List<string>>());

                RoadEvent r_e = new RoadEvent();
                int event_middle = i_start + Convert.ToInt32((i_stop - i_start) / 2);
                r_e.gps_alt = packets[event_middle].gps_alt;
                r_e.gps_long = packets[event_middle].gps_long;
                r_e.gps_speed = packets[event_middle].gps_speed;
                r_e.timecode = packets[event_middle].total_time;

                for (int j = i_start; j <= i_stop; j++)
                {
                    r_e.accelerations.Add(packets[j].acceleration);
                    r_e.positions.Add(packets[j].position);
                    r_e.velocitys.Add(packets[j].velocity);
                    r_e.time_sec.Add(packets[j].time);
                    r_e.duration += packets[j].time;

                    //list_for_pit_records[list_for_pit_records.Count - 1].Add(new List<string>());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].acceleration[0].ToString());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].acceleration[1].ToString());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].acceleration[2].ToString());

                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].position[0].ToString());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].position[1].ToString());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].position[2].ToString());

                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].velocity[0].ToString());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].velocity[1].ToString());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].velocity[2].ToString());

                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(packets[j].gps_speed.ToString());
                    //list_for_pit_records[list_for_pit_records.Count - 1][list_for_pit_records[list_for_pit_records.Count - 1].Count - 1].Add(r_e.timecode.ToString());
                }

                if (r_e.positions.Count() > 1)
                {
                    for (int j = 1; j < r_e.positions.Count(); j++)
                    {
                        float[] cur_pos_diff = new float[3];
                        float[] cur_vel_diff = new float[3];

                        cur_pos_diff[0] = r_e.positions[j][0] - r_e.positions[j - 1][0];
                        cur_pos_diff[1] = r_e.positions[j][1] - r_e.positions[j - 1][1];
                        cur_pos_diff[2] = r_e.positions[j][2] - r_e.positions[j - 1][2];

                        cur_vel_diff[0] = r_e.velocitys[j][0] - r_e.velocitys[j - 1][0];
                        cur_vel_diff[1] = r_e.velocitys[j][1] - r_e.velocitys[j - 1][1];
                        cur_vel_diff[2] = r_e.velocitys[j][2] - r_e.velocitys[j - 1][2];

                        r_e.positions_difference.Add(cur_pos_diff);
                        r_e.velocitys_difference.Add(cur_vel_diff);
                    }
                }
                else
                {
                    r_e.positions_difference.Add(r_e.positions[0]);
                    r_e.velocitys_difference.Add(r_e.velocitys[0]);
                }

                r_e.icon_size = (r_e.duration * r_e.gps_speed) / 2;
                //if (r_e.icon_size > 6) r_e.icon_size = 6;
                //if (r_e.icon_size <= 0) r_e.icon_size = 1;

                road_event_found_handler(r_e);

                packets.RemoveRange(0, i_stop+24);
                event_end_found = false;
                event_start_found = false;
                i_start = 16;
                i_stop = 0;
            }

            if(!event_end_found && !event_start_found &&  cur_pcount > 48)
            {
                packets.RemoveRange(0, cur_pcount - 32);
            }
        }
    }

    public class RoadEvent : RoadInterface
    {
        public float timecode { get; set; }
        public float duration { get; set; }
        public float icon_size { get; set; }

        public RoadEvent() : base()
        {
            timecode = 0;
            duration = 0;
            icon_size = 0;
        }
    }

    public class LearningData
    {
        public RoadEvent re { get; set; }
        public RoadObject ro { get; set; }
        public int driveid { get; set; }

        public LearningData()
        {
            re = null;
            ro = null;
            driveid = 0;
        }
    }
}