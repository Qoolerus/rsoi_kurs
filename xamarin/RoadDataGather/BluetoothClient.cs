using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Bluetooth;
using Java.Util;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using Android.Hardware;
using Android.Media;
using Java.Lang.Reflect;

namespace RoadDataGather
{
    public class BluetoothClient
    {
        //��� ����� ������� ��������
        private string mac;

        private BluetoothAdapter mBluetoothAdapter;
        private BluetoothSocket btSocket;
        private System.IO.Stream inStream = null;
        private System.IO.Stream outStream = null;

        private Activity mainActivity = null;

        public event EventHandler<string> DataRecieved;
        public event EventHandler<string> ConnectionFailed;
        public int connection_id;
        

        private void DataRecieved_handler(string data)
        {
            DataRecieved?.Invoke(this, data);
        }

        private void ConnectionFailed_handler(string error)
        {
            ConnectionFailed?.Invoke(this, error);
        }

        public BluetoothClient(string remote_device_mac, Activity main_activity)
        {
            mac = remote_device_mac;
            mBluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            mainActivity = main_activity;
        }

        private void Listen(string filepath = null)
        {
            try
            {
                outStream = null;
                inStream = btSocket.InputStream;
            }
            catch(Exception e) { ConnectionFailed_handler(e.Message); }

            //if (inStream != null)
                Task.Factory.StartNew(() => 
                {
                    if (filepath == null)
                    {
                        //listening from bluetooth
                        byte[] buffer = new byte[2048];
                        int bytes = 0;
                        while (true)
                        {
                            try
                            {

                                if (!inStream.CanRead || !inStream.IsDataAvailable())
                                {
                                    if (!inStream.CanRead)
                                    {
                                        break;
                                    }
                                    continue;
                                }


                                buffer[bytes] = (byte)inStream.ReadByte();
                                //counter = 0;
                                if (buffer[bytes] == '#' || buffer[bytes] == '|')
                                {
                                    var raw_data = new Java.Lang.String(buffer, 0, bytes);
                                    bytes = 0;

                                    DataRecieved_handler((string)raw_data);
                                }
                                else
                                {
                                    bytes++;
                                }
                            }
                            catch (Exception e)
                            {
                                ConnectionFailed_handler(e.Message);
                                break;
                            }
                        }
                    }
                    else
                    {
                        //listening from recorded data
                        StreamReader sr = new StreamReader(filepath);
                        string data = sr.ReadToEnd();
                        string[] data_splitted = data.Split('\n');
                        string current_data = "";
                        string[] c_d_r;
                        string data_to_send = "";
                        //int old_delay = 0;

                        for(int i = 0; i < data_splitted.Count(); i++)
                        {
                            try
                            {
                                //if (!btSocket.IsConnected)
                                //    break;

                                current_data = data_splitted[i];
                                c_d_r = current_data.Split(' ');

                                double current_delay = Convert.ToDouble(c_d_r[6].ToString().Replace('.', ',')) * 1000;

                                data_to_send = c_d_r[0].ToString().Replace('.', ',') + "/" + c_d_r[1].ToString().Replace('.', ',') + "/" +
                                c_d_r[2].ToString().Replace('.', ',') + "/" + c_d_r[3].ToString().Replace('.', ',') + "/" + c_d_r[4].ToString().Replace('.', ',') + "/" +
                                    c_d_r[5].ToString().Replace('.', ',') + "/" + (int)current_delay + "/"+ c_d_r[7].ToString().Replace('.', ',')+ "/"+
                                    c_d_r[8].ToString().Replace('.', ',') + "/" +c_d_r[9].ToString().Replace('.', ',');

                                DataRecieved_handler(data_to_send);

                                Thread.Sleep((int)current_delay);
                                //old_delay = current_delay;
                            }
                            catch(Exception e) { ConnectionFailed_handler(e.Message); }
                        }
                        ConnectionFailed_handler("Record ended.");
                    }
                });

        }

        private void Write(Java.Lang.String message)
        {
            try
            {
                outStream = btSocket.OutputStream;
            }
            catch { }

            Java.Lang.String message_local = message;

            byte[] msgBuffer = message_local.GetBytes();

            try
            {
                outStream.Write(msgBuffer, 0, msgBuffer.Length);
            }
            catch { }
        }

        public void Connect(string filepath = null)
        {
            if (!mBluetoothAdapter.IsEnabled)
            {
                var enableBtIntent = new Intent(BluetoothAdapter.ActionRequestEnable);
                mainActivity.StartActivityForResult(enableBtIntent, 2);
            }

            BluetoothDevice device = mBluetoothAdapter.GetRemoteDevice(mac);
            mBluetoothAdapter.CancelDiscovery();

            try
            {
                var ids = device.GetUuids();
                if ((ids != null) && (ids.Length > 0))
                {
                    for (int i = 0; i < ids.Count(); i++)
                    {
                        try
                        {
                            var id = ids[i];
                            btSocket = device.CreateRfcommSocketToServiceRecord(id.Uuid);
                            btSocket.Connect();
                            break;
                        }
                        catch (Exception e)
                        {
                            if (i == ids.Count() - 1)
                                throw new Exception("Correct uuid not found. " + e.Message);
                        }
                    }
                }

                //Listen(filepath);
                System.Random r = new System.Random();
                connection_id = r.Next(11111, 99999);
                Listen();
            }
            catch (Exception e)
            {
                ConnectionFailed_handler(e.Message); 
                if (btSocket != null)
                    btSocket.Close();
            }
        }

        public void Disconnect()
        {
            if (btSocket.IsConnected)
            {
                try
                {
                    btSocket.Close();
                }
                catch { }
            }
        }
    }
}