using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Math.Optimization.Losses;
using Accord.MachineLearning.DecisionTrees;

namespace RoadDataGather
{
    public class RoadObjectsClassificator
    {
        public List<RoadObject> road_objects { get; set; }
        public List<RoadObject> testGlobalSample { get; set; }
        public List<RoadObject> testTestSample { get; set; }
        private MainActivity main_activ { get; set; }
        Random r = new Random();

        private float acc_weight = 1;
        private float vel_weight = 0.7f;
        private float pos_weight = 0.7f;
        private float dur_weight = 0.8f;

        private MultipleLinearRegression regression;
        private RandomForest forest;

        public RoadObjectsClassificator(MainActivity m)
        {
            main_activ = m;

            road_objects = LoadRoadObjectsBase(Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/pits_records.txt");
            //testGlobalSample = LoadRoadObjectsBase(Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/globalSample.txt");
            //testTestSample = LoadRoadObjectsBase(Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/testSample.txt");

            //initMultiLinearRegression();
            initRandomForest();
        }

        private List<RoadObject> LoadRoadObjectsBase(string path)
        {
            StreamReader sr = new StreamReader(path);
            List<RoadObject> objectsList = new List<RoadObject>();

            string ro_base = "";
            main_activ.RunOnUiThread(() => {
                ro_base = sr.ReadToEnd();
                sr.Close();
            });

            string[] stringSeparators = new string[] { "\n\n" };
            string[] ro_base_spl = ro_base.Split(stringSeparators, StringSplitOptions.None);

            stringSeparators = new string[] { "\n" };
            for (int i = 0; i < ro_base_spl.Count(); i++)
            {
                string[] name_and_parts = ro_base_spl[i].Split(stringSeparators, StringSplitOptions.None);
                string[] name = name_and_parts[0].Split('/');

                RoadObject new_road_obj = new RoadObject();
                new_road_obj.name = name[0];
                new_road_obj.quality = Convert.ToInt32(name[1]);
                new_road_obj.color = name[2];
                new_road_obj.icon_size = float.Parse(name[3].ToString().Replace('.',','));

                for (int j = 1; j < name_and_parts.Count(); j++)
                {
                    string[] part_spl = name_and_parts[j].Split('/');

                    float[] acc = new float[3];
                    float.TryParse(part_spl[0], out acc[0]);
                    float.TryParse(part_spl[1], out acc[1]);
                    float.TryParse(part_spl[2], out acc[2]);

                    float[] pos = new float[3];
                    float.TryParse(part_spl[3], out pos[0]);
                    float.TryParse(part_spl[4], out pos[1]);
                    float.TryParse(part_spl[5], out pos[2]);

                    float[] vel = new float[3];
                    float.TryParse(part_spl[6], out vel[0]);
                    float.TryParse(part_spl[7], out vel[1]);
                    float.TryParse(part_spl[8], out vel[2]);

                    new_road_obj.accelerations.Add(acc);
                    new_road_obj.positions.Add(pos);
                    new_road_obj.velocitys.Add(vel);

                    float speed = 0;
                    float.TryParse(part_spl[9], out speed);
                    new_road_obj.gps_speed = speed;
                }

                if (new_road_obj.positions.Count() > 1)
                {
                    for (int j = 1; j < new_road_obj.positions.Count(); j++)
                    {
                        float[] cur_pos_diff = new float[3];
                        float[] cur_vel_diff = new float[3];

                        cur_pos_diff[0] = new_road_obj.positions[j][0] - new_road_obj.positions[j - 1][0];
                        cur_pos_diff[1] = new_road_obj.positions[j][1] - new_road_obj.positions[j - 1][1];
                        cur_pos_diff[2] = new_road_obj.positions[j][2] - new_road_obj.positions[j - 1][2];

                        cur_vel_diff[0] = new_road_obj.velocitys[j][0] - new_road_obj.velocitys[j - 1][0];
                        cur_vel_diff[1] = new_road_obj.velocitys[j][1] - new_road_obj.velocitys[j - 1][1];
                        cur_vel_diff[2] = new_road_obj.velocitys[j][2] - new_road_obj.velocitys[j - 1][2];

                        new_road_obj.positions_difference.Add(cur_pos_diff);
                        new_road_obj.velocitys_difference.Add(cur_vel_diff);
                    }
                }
                else
                {
                    new_road_obj.positions_difference.Add(new_road_obj.positions[0]);
                    new_road_obj.velocitys_difference.Add(new_road_obj.velocitys[0]);
                }

                //if(new_road_obj.accelerations.Count > 1)
                objectsList.Add(new_road_obj);
            }

            return objectsList;
        }

        public void initRandomForest()
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            System.Diagnostics.Stopwatch stopwatch2 = new System.Diagnostics.Stopwatch();

            stopwatch.Start();
            Accord.Math.Random.Generator.Seed = 1;

            double[][] inputs = new double[road_objects.Count][];
            int[] outputs = new int[road_objects.Count];

            for (int i = 0; i < road_objects.Count; i++)
            {
                outputs[i] = road_objects[i].quality;

                List<float> signs = road_objects[i].getSigns();
                inputs[i] = new double[signs.Count];

                for (int j = 0; j < signs.Count; j++)
                    inputs[i][j] = signs[j];
            }

            //testing
            //double[][] globalSampleInputs = new double[testGlobalSample.Count][];
            //int[] globalSampleOutputs = new int[testGlobalSample.Count];
            //for (int i = 0; i < testGlobalSample.Count; i++)
            //{
            //    globalSampleOutputs[i] = testGlobalSample[i].quality;

            //    List<float> signs = testGlobalSample[i].getSigns();
            //    globalSampleInputs[i] = new double[signs.Count];

            //    for (int j = 0; j < signs.Count; j++)
            //        globalSampleInputs[i][j] = signs[j];
            //}

            //double[][] testSampleInputs = new double[testTestSample.Count][];
            //int[] testSampleOutputs = new int[testTestSample.Count];
            //for (int i = 0; i < testTestSample.Count; i++)
            //{
            //    testSampleOutputs[i] = testTestSample[i].quality;

            //    List<float> signs = testTestSample[i].getSigns();
            //    testSampleInputs[i] = new double[signs.Count];

            //    for (int j = 0; j < signs.Count; j++)
            //        testSampleInputs[i][j] = signs[j];
            //}
            ////var teacher = new RandomForestLearningFixed();
            ////teacher.NumberOfTrees = 1;
            ////teacher.SampleRatio = 0.6;
            ////teacher.CoverageRatio = 0.1;

            ////forest = teacher.Learn(globalSampleInputs, globalSampleOutputs);

            ////int[] prediction = forest.Decide(testSampleInputs);

            ////double error = new SquareLoss(expected: testSampleOutputs.Select(val => (double)val).ToArray()).Loss(actual: prediction.Select(val => (double)val).ToArray());

            ////matlabdata saving
            //StreamWriter swGlobal = new StreamWriter(Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/matlabGlobal.txt");
            //for (int i = 0; i < testGlobalSample.Count; i++)
            //{
            //    var output = testGlobalSample[i].quality;
            //    List<float> signs = testGlobalSample[i].getSigns();

            //    string row = signs[0].ToString() + " " + signs[1].ToString() + " " + signs[16].ToString() + " " + signs[42].ToString() + " " + signs[64].ToString() + " " +
            //        signs[71].ToString() + " " + output;
            //    swGlobal.WriteLine(row);
            //}

            //StreamWriter swTest = new StreamWriter(Android.OS.Environment.ExternalStorageDirectory.Path + "/road_scanner/matlabTest.txt");
            //for (int i = 0; i < testTestSample.Count; i++)
            //{
            //    var output = testTestSample[i].quality;
            //    List<float> signs = testTestSample[i].getSigns();

            //    string row = signs[0].ToString() + " " + signs[1].ToString() + " " + signs[16].ToString() + " " + signs[42].ToString() + " " + signs[64].ToString() + " " +
            //        signs[71].ToString() + " " + output;
            //    swTest.WriteLine(row);
            //}

            //swGlobal.Close();
            //swTest.Close();

            //testing complete

            var teacher = new RandomForestLearningFixed();
            teacher.NumberOfTrees = 80;
            teacher.SampleRatio = 0.6;
            teacher.CoverageRatio = 0.2;

            forest = teacher.Learn(inputs, outputs);

            stopwatch2.Start();
            int[] prediction = forest.Decide(inputs);
            stopwatch2.Stop();
            var time2 = stopwatch2.ElapsedMilliseconds;

            double error = new SquareLoss(expected: outputs.Select(val => (double)val).ToArray()).Loss(actual: prediction.Select(val => (double)val).ToArray());

            stopwatch.Stop();
            var time = stopwatch.ElapsedMilliseconds;
        }

        public void initMultiLinearRegression()
        {
            if (road_objects.Count > 0)
            {
                System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

                var ols = new OrdinaryLeastSquares()
                {
                    UseIntercept = true
                };

                double[][] inputs = new double[road_objects.Count][];
                double[] outputs = new double[road_objects.Count];

                for (int i = 0; i < road_objects.Count; i++)
                {
                    outputs[i] = road_objects[i].quality;

                    List<float> signs = road_objects[i].getSigns();
                    inputs[i] = new double[signs.Count];

                    for (int j = 0; j < signs.Count; j++)
                        inputs[i][j] = signs[j];
                }

                //testing linearRegr quality
                //double[][] globalSampleInputs = new double[testGlobalSample.Count][];
                //double[] globalSampleOutputs = new double[testGlobalSample.Count];
                //for (int i = 0; i < testGlobalSample.Count; i++)
                //{
                //    globalSampleOutputs[i] = testGlobalSample[i].quality;

                //    List<float> signs = testGlobalSample[i].getSigns();
                //    globalSampleInputs[i] = new double[signs.Count];

                //    for (int j = 0; j < signs.Count; j++)
                //        globalSampleInputs[i][j] = signs[j];
                //}

                //double[][] testSampleInputs = new double[testTestSample.Count][];
                //double[] testSampleOutputs = new double[testTestSample.Count];
                //for (int i = 0; i < testTestSample.Count; i++)
                //{
                //    testSampleOutputs[i] = testTestSample[i].quality;

                //    List<float> signs = testTestSample[i].getSigns();
                //    testSampleInputs[i] = new double[signs.Count];

                //    for (int j = 0; j < signs.Count; j++)
                //        testSampleInputs[i][j] = signs[j];
                //}

                //regression = ols.Learn(globalSampleInputs, globalSampleOutputs);
                //var standartError = regression.GetStandardError(globalSampleInputs, globalSampleOutputs);
                //double[] prediction = regression.Transform(testSampleInputs);

                //double error = new SquareLoss(expected: testSampleOutputs).Loss(actual: prediction);

                //testing complete


                regression = ols.Learn(inputs, outputs);
                double[] coefs = regression.Weights;

                stopwatch.Start();
                double[] prediction = regression.Transform(inputs);
                stopwatch.Stop();
                var time = stopwatch.ElapsedMilliseconds;

                double error = new SquareLoss(expected: outputs).Loss(actual: prediction);
            }
        }

        public RoadObject ClassificateObject()
        {
            return new RoadObject();
        }

        public void ClassificateByRandomForest(RoadEvent road_event, EventHandler<RoadObject> callback)
        {
            RoadObject classificated_object = new RoadObject();

            classificated_object.gps_alt = road_event.gps_alt;
            classificated_object.gps_long = road_event.gps_long;
            classificated_object.gps_speed = road_event.gps_speed;

            if (road_objects.Count != 0)
            {
                var r_e_signs = road_event.getSigns();
                double[] inputs = new double[r_e_signs.Count];

                for (int i = 0; i < r_e_signs.Count; i++)
                    inputs[i] = r_e_signs[i];

                double predicted_quality = forest.Decide(inputs);

                for (var i = 0; i < road_objects.Count; i++)
                    if (road_objects[i].quality == predicted_quality)
                    {
                        classificated_object.name = road_objects[i].name;
                        classificated_object.color = road_objects[i].color;
                        classificated_object.icon_size = road_event.icon_size;
                        classificated_object.quality = road_objects[i].quality;
                        classificated_object.kk = (float)predicted_quality;
                        callback(this, classificated_object);
                        break;
                    }
            }
            else
            {
                callback(this, null);
            }
        }

        public void ClassificateByRegression(RoadEvent road_event, EventHandler<RoadObject> callback)
        {
            RoadObject classificated_object = new RoadObject();

            classificated_object.gps_alt = road_event.gps_alt;
            classificated_object.gps_long = road_event.gps_long;
            classificated_object.gps_speed = road_event.gps_speed;

            if (road_objects.Count != 0)
            {
                var r_e_signs = road_event.getSigns();
                double[] inputs = new double[r_e_signs.Count];

                for (int i = 0; i < r_e_signs.Count; i++)
                    inputs[i] = r_e_signs[i];

                double predicted_quality = regression.Transform(inputs);

                for (var i = 0; i < road_objects.Count; i++)
                    if (road_objects[i].quality == Math.Round(predicted_quality))
                    {
                        classificated_object.name = road_objects[i].name;
                        classificated_object.color = road_objects[i].color;
                        classificated_object.icon_size = road_event.icon_size;
                        classificated_object.quality = road_objects[i].quality;
                        classificated_object.kk = (float)predicted_quality;
                        callback(this, classificated_object);
                        break;
                    }
            }
            else
            {
                callback(this, null);
            }
        }

        public void Classificate(RoadEvent road_event, EventHandler<RoadObject> callback)
        {
            RoadObject classificated_object = new RoadObject();

            int minus = r.Next(0, 10) <= 5 ? 1 : -1;
            classificated_object.gps_alt = road_event.gps_alt;// + minus*((float)r.NextDouble() * (0.00002f - 0.000001f) + 0.000001f);
            classificated_object.gps_long = road_event.gps_long;// + minus*((float)r.NextDouble() * (0.00002f - 0.000001f) + 0.000001f);
            classificated_object.gps_speed = road_event.gps_speed;

            if (road_objects.Count != 0)
            {
                float kk = 0;
                string obj = "";
                string color = "";
                float size = 0;
                int quality = 0;
                for(int i = 0; i < road_objects.Count; i++)
                {
                    if (Math.Abs(road_objects[i].gps_speed - road_event.gps_speed) > 5)
                        continue;

                    float acc_kk = 0;
                    float pos_kk = 0;
                    float vel_kk = 0;
                    float dur_kk = 0;

                    //kk for accelerations
                    float acc_scalar_mult_x = 0, acc_modul_object_x = 0, acc_modul_event_x = 0;
                    float acc_scalar_mult_y = 0, acc_modul_object_y = 0, acc_modul_event_y = 0;
                    float acc_scalar_mult_z = 0, acc_modul_object_z = 0, acc_modul_event_z = 0;
                    for (int j = 0; j < Math.Min(road_objects[i].accelerations.Count, road_event.accelerations.Count); j++)
                    {
                        acc_scalar_mult_x += road_objects[i].accelerations[j][0] * road_event.accelerations[j][0];
                        acc_scalar_mult_y += road_objects[i].accelerations[j][1] * road_event.accelerations[j][1];
                        acc_scalar_mult_z += road_objects[i].accelerations[j][2] * road_event.accelerations[j][2];

                        acc_modul_object_x += (float)Math.Pow(road_objects[i].accelerations[j][0], 2);
                        acc_modul_event_x += (float)Math.Pow(road_event.accelerations[j][0], 2);

                        acc_modul_object_y += (float)Math.Pow(road_objects[i].accelerations[j][1], 2);
                        acc_modul_event_y += (float)Math.Pow(road_event.accelerations[j][1], 2);

                        acc_modul_object_z += (float)Math.Pow(road_objects[i].accelerations[j][2], 2);
                        acc_modul_event_z += (float)Math.Pow(road_event.accelerations[j][2], 2);
                    }

                    acc_modul_object_x = (float)Math.Sqrt(acc_modul_object_x);
                    acc_modul_event_x = (float)Math.Sqrt(acc_modul_event_x);

                    acc_modul_object_y = (float)Math.Sqrt(acc_modul_object_y);
                    acc_modul_event_y = (float)Math.Sqrt(acc_modul_event_y);

                    acc_modul_object_z = (float)Math.Sqrt(acc_modul_object_z);
                    acc_modul_event_z = (float)Math.Sqrt(acc_modul_event_z);

                    float modul_multi_x = acc_modul_object_x * acc_modul_event_x;
                    float modul_multi_y = acc_modul_object_y * acc_modul_event_y;
                    float modul_multi_z = acc_modul_object_z * acc_modul_event_z;

                    float acc_kk_x = 0, acc_kk_y = 0, acc_kk_z = 0;

                    if (modul_multi_x != 0)
                        acc_kk_x = acc_scalar_mult_x / modul_multi_x;
                    if (modul_multi_y != 0)
                        acc_kk_y = acc_scalar_mult_y / modul_multi_y;
                    if (modul_multi_z != 0)
                        acc_kk_z = acc_scalar_mult_z / modul_multi_z;

                    acc_kk = (acc_kk_x + acc_kk_y + acc_kk_z) / 3;

                    //kk for posistion
                    float pos_scalar_mult_x = 0, pos_modul_object_x = 0, pos_modul_event_x = 0;
                    float pos_scalar_mult_y = 0, pos_modul_object_y = 0, pos_modul_event_y = 0;
                    float pos_scalar_mult_z = 0, pos_modul_object_z = 0, pos_modul_event_z = 0;
                    for (int j = 0; j < Math.Min(road_objects[i].positions_difference.Count, road_event.positions_difference.Count); j++)
                    {
                        pos_scalar_mult_x += road_objects[i].positions_difference[j][0] * road_event.positions_difference[j][0];
                        pos_scalar_mult_y += road_objects[i].positions_difference[j][1] * road_event.positions_difference[j][1];
                        pos_scalar_mult_z += road_objects[i].positions_difference[j][2] * road_event.positions_difference[j][2];

                        pos_modul_object_x += (float)Math.Pow(road_objects[i].positions_difference[j][0], 2);
                        pos_modul_event_x += (float)Math.Pow(road_event.positions_difference[j][0], 2);

                        pos_modul_object_y += (float)Math.Pow(road_objects[i].positions_difference[j][1], 2);
                        pos_modul_event_y += (float)Math.Pow(road_event.positions_difference[j][1], 2);

                        pos_modul_object_z += (float)Math.Pow(road_objects[i].positions_difference[j][2], 2);
                        pos_modul_event_z += (float)Math.Pow(road_event.positions_difference[j][2], 2);
                    }

                    pos_modul_object_x = (float)Math.Sqrt(pos_modul_object_x);
                    pos_modul_event_x = (float)Math.Sqrt(pos_modul_event_x);

                    pos_modul_object_y = (float)Math.Sqrt(pos_modul_object_y);
                    pos_modul_event_y = (float)Math.Sqrt(pos_modul_event_y);

                    pos_modul_object_z = (float)Math.Sqrt(pos_modul_object_z);
                    pos_modul_event_z = (float)Math.Sqrt(pos_modul_event_z);

                    float pos_modul_multi_x = pos_modul_object_x * pos_modul_event_x;
                    float pos_modul_multi_y = pos_modul_object_y * pos_modul_event_y;
                    float pos_modul_multi_z = pos_modul_object_z * pos_modul_event_z;

                    float pos_kk_x = 0, pos_kk_y = 0, pos_kk_z = 0;

                    if (pos_modul_multi_x != 0)
                        pos_kk_x = pos_scalar_mult_x / pos_modul_multi_x;
                    if (pos_modul_multi_y != 0)
                        pos_kk_y = pos_scalar_mult_y / pos_modul_multi_y;
                    if (pos_modul_multi_z != 0)
                        pos_kk_z = pos_scalar_mult_z / pos_modul_multi_z;

                    pos_kk = (pos_kk_x + pos_kk_y + pos_kk_z) / 3;

                    //kk for velocity
                    float vel_scalar_mult_x = 0, vel_modul_object_x = 0, vel_modul_event_x = 0;
                    float vel_scalar_mult_y = 0, vel_modul_object_y = 0, vel_modul_event_y = 0;
                    float vel_scalar_mult_z = 0, vel_modul_object_z = 0, vel_modul_event_z = 0;
                    for (int j = 0; j < Math.Min(road_objects[i].velocitys_difference.Count, road_event.velocitys_difference.Count); j++)
                    {
                        vel_scalar_mult_x += road_objects[i].velocitys_difference[j][0] * road_event.velocitys_difference[j][0];
                        vel_scalar_mult_y += road_objects[i].velocitys_difference[j][1] * road_event.velocitys_difference[j][1];
                        vel_scalar_mult_z += road_objects[i].velocitys_difference[j][2] * road_event.velocitys_difference[j][2];

                        vel_modul_object_x += (float)Math.Pow(road_objects[i].velocitys_difference[j][0], 2);
                        vel_modul_event_x += (float)Math.Pow(road_event.velocitys_difference[j][0], 2);

                        vel_modul_object_y += (float)Math.Pow(road_objects[i].velocitys_difference[j][1], 2);
                        vel_modul_event_y += (float)Math.Pow(road_event.velocitys_difference[j][1], 2);

                        vel_modul_object_z += (float)Math.Pow(road_objects[i].velocitys_difference[j][2], 2);
                        vel_modul_event_z += (float)Math.Pow(road_event.velocitys_difference[j][2], 2);
                    }

                    vel_modul_object_x = (float)Math.Sqrt(vel_modul_object_x);
                    vel_modul_event_x = (float)Math.Sqrt(vel_modul_event_x);

                    vel_modul_object_y = (float)Math.Sqrt(vel_modul_object_y);
                    vel_modul_event_y = (float)Math.Sqrt(vel_modul_event_y);

                    vel_modul_object_z = (float)Math.Sqrt(vel_modul_object_z);
                    vel_modul_event_z = (float)Math.Sqrt(vel_modul_event_z);

                    float vel_modul_multi_x = vel_modul_object_x * vel_modul_event_x;
                    float vel_modul_multi_y = vel_modul_object_y * vel_modul_event_y;
                    float vel_modul_multi_z = vel_modul_object_z * vel_modul_event_z;

                    float vel_kk_x = 0, vel_kk_y = 0, vel_kk_z = 0;

                    if (vel_modul_multi_x != 0)
                        vel_kk_x = vel_scalar_mult_x / vel_modul_multi_x;
                    if (vel_modul_multi_y != 0)
                        vel_kk_y = vel_scalar_mult_y / vel_modul_multi_y;
                    if (vel_modul_multi_z != 0)
                        vel_kk_z = vel_scalar_mult_z / vel_modul_multi_z;

                    vel_kk = (vel_kk_x + vel_kk_y + vel_kk_z) / 3;

                    // kk for duration 
                    float a = road_objects[i].accelerations.Count;
                    float b = road_event.accelerations.Count;
                    if(b > a)
                    {
                        float c = a;
                        a = b;
                        b = c;
                    }

                    dur_kk = 1 - ((a - b) / a);
                    if (dur_kk > 1)
                        dur_kk = 1;
                    if (dur_kk < 0)
                        dur_kk = 0;

                    //summary kk
                    float new_kk = (acc_kk*acc_weight + pos_kk*pos_weight + vel_kk*vel_weight + dur_kk*dur_weight) / 4;

                    if(new_kk > kk)
                    {
                        kk = new_kk;
                        obj = road_objects[i].name;
                        color = road_objects[i].color;
                        size = road_event.icon_size;
                        quality = road_objects[i].quality;
                    }
                }


                classificated_object.name = obj;
                classificated_object.kk = kk;
                classificated_object.quality = quality;
                classificated_object.color = color;
                classificated_object.icon_size = size;

                callback(this, classificated_object);
            }
            else
                callback(this, null);
        }
    }

    public class RoadInterface
    {
        public List<float> time_sec { get; set; }
        public List<float[]> accelerations { get; set; }
        public List<float[]> velocitys { get; set; }
        public List<float[]> positions { get; set; }
        public float gps_alt { get; set; }
        public float gps_long { get; set; }
        public float gps_speed { get; set; }
        public List<float[]> positions_difference { get; set; }
        public List<float[]> velocitys_difference { get; set; }

        public RoadInterface()
        {
            time_sec = new List<float>();
            accelerations = new List<float[]>();
            velocitys = new List<float[]>();
            positions = new List<float[]>();
            gps_alt = 0;
            gps_long = 0;
            gps_speed = 0;
            positions_difference = new List<float[]>();
            velocitys_difference = new List<float[]>();
        }

        public List<float> getSigns()
        {
            List<float> signs = new List<float>();

            //---accelerometer signs---
            // Mean
            float[] acc_mean = Mean(accelerations);
            // Std
            float[] acc_std = Std(accelerations);
            // Skz
            float[] acc_skz = Skz(accelerations);
            // Min
            float[] acc_min = Min(accelerations);
            // Max
            float[] acc_max = Max(accelerations);
            // Skewnes(����������)
            float[] acc_skewnews = Skewnes(accelerations);
            // Kurtosis(��������)
            float[] acc_kurtosis = Kurtosis(accelerations);
            // Magnitude
            float acc_magnitude = Magnitude(accelerations);
            // Magnitude Area
            float acc_magnitude_area = MagnitudeArea(accelerations);
            // Absolute Difference
            float acc_abs_diff = AbsoluteDifference(accelerations);
            // Correlations
            float[] acc_correls = �orrelation(accelerations);
            // Quantels
            float[] acc_quantels = Quantels(accelerations);

            //---velocity signs---
            // Mean
            float[] vel_mean = Mean(velocitys);
            // Std
            float[] vel_std = Std(velocitys);
            // Min
            float[] vel_min = Min(velocitys);
            // Max
            float[] vel_max = Max(velocitys);
            // Diffs Mean
            float[] vel_diff_mean = Mean(velocitys_difference);
            // Diffs Std
            float[] vel_diff_std = Std(velocitys_difference);

            //---position signs---
            // Mean
            float[] pos_mean = Mean(positions);
            // Std
            float[] pos_std = Std(positions);
            // Min
            float[] pos_min = Min(positions);
            // Max
            float[] pos_max = Max(positions);
            // Diffs Mean
            float[] pos_diff_mean = Mean(positions_difference);
            // Diffs Std
            float[] pos_diff_std = Std(positions_difference);

            //  Adding signs to vector
            signs.Add(acc_mean[0]); signs.Add(acc_mean[1]); signs.Add(acc_mean[2]);
            signs.Add(acc_std[0]); signs.Add(acc_std[1]); signs.Add(acc_std[2]);
            signs.Add(acc_skz[0]); signs.Add(acc_skz[1]); signs.Add(acc_skz[2]);
            signs.Add(acc_min[0]); signs.Add(acc_min[1]); signs.Add(acc_min[2]);
            signs.Add(acc_max[0]); signs.Add(acc_max[1]); signs.Add(acc_max[2]);
            signs.Add(acc_skewnews[0]); signs.Add(acc_skewnews[1]); signs.Add(acc_skewnews[2]);
            signs.Add(acc_kurtosis[0]); signs.Add(acc_kurtosis[1]); signs.Add(acc_kurtosis[2]);
            signs.Add(acc_magnitude); signs.Add(acc_magnitude_area); signs.Add(acc_abs_diff);
            signs.Add(acc_correls[0]); signs.Add(acc_correls[1]); signs.Add(acc_correls[2]);
            //adding quantels
            for (int i = 0; i < acc_quantels.Length; i++)
                signs.Add(acc_quantels[i]);
            signs.Add(vel_mean[0]); signs.Add(vel_mean[1]); signs.Add(vel_mean[2]);
            signs.Add(vel_std[0]); signs.Add(vel_std[1]); signs.Add(vel_std[2]);
            signs.Add(vel_min[0]); signs.Add(vel_min[1]); signs.Add(vel_min[2]);
            signs.Add(vel_max[0]); signs.Add(vel_max[1]); signs.Add(vel_max[2]);
            signs.Add(vel_diff_mean[0]); signs.Add(vel_diff_mean[1]); signs.Add(vel_diff_mean[2]);
            signs.Add(vel_diff_std[0]); signs.Add(vel_diff_std[1]); signs.Add(vel_diff_std[2]);
            signs.Add(pos_mean[0]); signs.Add(pos_mean[1]); signs.Add(pos_mean[2]);
            signs.Add(pos_std[0]); signs.Add(pos_std[1]); signs.Add(pos_std[2]);
            signs.Add(pos_min[0]); signs.Add(pos_min[1]); signs.Add(pos_min[2]);
            signs.Add(pos_max[0]); signs.Add(pos_max[1]); signs.Add(pos_max[2]);
            signs.Add(pos_diff_mean[0]); signs.Add(pos_diff_mean[1]); signs.Add(pos_diff_mean[2]);
            signs.Add(pos_diff_std[0]); signs.Add(pos_diff_std[1]); signs.Add(pos_diff_std[2]);
            signs.Add(gps_speed);

            return signs;
        }

        private static float[] Mean(List<float[]> series)
        {
            float[] avg = new float[3] { 0, 0, 0 };

            for(int i = 0; i < series.Count; i++)
            {
                avg[0] += series[i][0];
                avg[1] += series[i][1];
                avg[2] += series[i][2];
            }

            avg[0] /= series.Count;
            avg[1] /= series.Count;
            avg[2] /= series.Count;

            return avg;
        }

        private static float[] Std(List<float[]> series)
        {
            if (series.Count <= 1)
                return new float[] { 0, 0, 0 };

            float[] avg = Mean(series);
            float[] std = new float[3] { 0, 0, 0 };

            for (int i = 0; i < series.Count; i++)
            {
                std[0] += (float)Math.Pow(series[i][0] - avg[0], 2);
                std[1] += (float)Math.Pow(series[i][1] - avg[1], 2);
                std[2] += (float)Math.Pow(series[i][2] - avg[2], 2);
            }

            std[0] /= (series.Count - 1);
            std[1] /= (series.Count - 1);
            std[2] /= (series.Count - 1);

            return std;
        }

        private static float[] Min(List<float[]> series)
        {
            var mins = new float[3] { series[0][0], series[0][1], series[0][2] };

            for(int i = 1; i < series.Count; i++)
            {
                if (mins[0] > series[i][0])
                    mins[0] = series[i][0];
                if (mins[1] > series[i][1])
                    mins[1] = series[i][1];
                if (mins[2] > series[i][2])
                    mins[2] = series[i][2];
            }

            return mins;
        }

        private static float[] Max(List<float[]> series)
        {
            var maxes = new float[3] { series[0][0], series[0][1], series[0][2] };

            for (int i = 1; i < series.Count; i++)
            {
                if (maxes[0] < series[i][0])
                    maxes[0] = series[i][0];
                if (maxes[1] < series[i][1])
                    maxes[1] = series[i][1];
                if (maxes[2] < series[i][2])
                    maxes[2] = series[i][2];
            }

            return maxes;
        }

        private static float[] Skz(List<float[]> series)
        {
            List<float> x_extremums = new List<float>();
            List<float> y_extremums = new List<float>();
            List<float> z_extremums = new List<float>();
            bool[] extr_find_flag = new bool[3] { false, false, false };
            float[] skz = new float[3] { 0, 0, 0 };

            // Finding extremums
            if (series.Count > 1)
            {
                for (int i = 1; i < series.Count; i++)
                {
                    float[] a_cur = new float[3] { Math.Abs(series[i][0]), Math.Abs(series[i][1]), Math.Abs(series[i][2]) };
                    float[] a_prev = new float[3] { Math.Abs(series[i - 1][0]), Math.Abs(series[i - 1][1]), Math.Abs(series[i - 1][2]) };

                    // X
                    if (a_cur[0] > a_prev[0])
                    {
                        extr_find_flag[0] = false;
                    }
                    else
                    {
                        if (extr_find_flag[0] == false)
                            x_extremums.Add(a_prev[0]);
                        extr_find_flag[0] = true;
                    }

                    // Y
                    if (a_cur[1] > a_prev[1])
                    {
                        extr_find_flag[1] = false;
                    }
                    else
                    {
                        if (extr_find_flag[1] == false)
                            y_extremums.Add(a_prev[1]);
                        extr_find_flag[1] = true;
                    }

                    // Z
                    if (a_cur[2] > a_prev[2])
                    {
                        extr_find_flag[2] = false;
                    }
                    else
                    {
                        if (extr_find_flag[2] == false)
                            z_extremums.Add(a_prev[2]);
                        extr_find_flag[2] = true;
                    }
                }

                if (extr_find_flag[0] == false)
                    x_extremums.Add(Math.Abs(series[series.Count - 1][0]));

                if (extr_find_flag[1] == false)
                    y_extremums.Add(Math.Abs(series[series.Count - 1][1]));

                if (extr_find_flag[2] == false)
                    z_extremums.Add(Math.Abs(series[series.Count - 1][2]));
            }
            else
            {
                x_extremums.Add(Math.Abs(series[0][0]));
                y_extremums.Add(Math.Abs(series[0][1]));
                z_extremums.Add(Math.Abs(series[0][2]));
            }

            // Calculating SKZ
            for (int i = 0; i < x_extremums.Count; i++)
                skz[0] += (float)Math.Pow(x_extremums[i], 2);
            for (int i = 0; i < y_extremums.Count; i++)
                skz[1] += (float)Math.Pow(y_extremums[i], 2);
            for (int i = 0; i < z_extremums.Count; i++)
                skz[2] += (float)Math.Pow(z_extremums[i], 2);

            skz[0] = (float)Math.Sqrt(skz[0] / x_extremums.Count);
            skz[1] = (float)Math.Sqrt(skz[1] / y_extremums.Count);
            skz[2] = (float)Math.Sqrt(skz[2] / z_extremums.Count);

            return skz;
        }

        private static float[] Skewnes(List<float[]> series)
        {
            if (series.Count <= 1)
                return new float[] { 0, 0, 0 };

            var avg = Mean(series);
            var uppart = new float[3] { 0, 0, 0 };
            var dwnpart = new float[3] { 0, 0, 0 };

            for (int i = 0; i < series.Count; i++)
            {
                uppart[0] += (float)Math.Pow(series[i][0] - avg[0], 3);
                uppart[1] += (float)Math.Pow(series[i][1] - avg[1], 3);
                uppart[2] += (float)Math.Pow(series[i][2] - avg[2], 3);

                dwnpart[0] += (float)Math.Pow(series[i][0] - avg[0], 2);
                dwnpart[1] += (float)Math.Pow(series[i][1] - avg[1], 2);
                dwnpart[2] += (float)Math.Pow(series[i][2] - avg[2], 2);
            }

            uppart[0] /= series.Count;
            uppart[1] /= series.Count;
            uppart[2] /= series.Count;

            dwnpart[0] = (float)Math.Pow(dwnpart[0] / series.Count, 1.5);
            dwnpart[1] = (float)Math.Pow(dwnpart[1] / series.Count, 1.5);
            dwnpart[2] = (float)Math.Pow(dwnpart[2] / series.Count, 1.5);

            if (dwnpart[0] == 0 || dwnpart[1] == 0 || dwnpart[2] == 0)
                return new float[] { 0, 0, 0 };

            return new float[3] { uppart[0]/dwnpart[0], uppart[1] / dwnpart[1], uppart[2] / dwnpart[2] };
        }

        private static float[] Kurtosis(List<float[]> series)
        {
            if (series.Count <= 1)
                return new float[] { 0, 0, 0 };

            var avg = Mean(series);
            var uppart = new float[3] { 0, 0, 0 };
            var dwnpart = new float[3] { 0, 0, 0 };

            for (int i = 0; i < series.Count; i++)
            {
                uppart[0] += (float)Math.Pow(series[i][0] - avg[0], 4);
                uppart[1] += (float)Math.Pow(series[i][1] - avg[1], 4);
                uppart[2] += (float)Math.Pow(series[i][2] - avg[2], 4);

                dwnpart[0] += (float)Math.Pow(series[i][0] - avg[0], 2);
                dwnpart[1] += (float)Math.Pow(series[i][1] - avg[1], 2);
                dwnpart[2] += (float)Math.Pow(series[i][2] - avg[2], 2);
            }

            uppart[0] /= series.Count;
            uppart[1] /= series.Count;
            uppart[2] /= series.Count;

            dwnpart[0] = (float)Math.Pow(dwnpart[0] / series.Count, 2);
            dwnpart[1] = (float)Math.Pow(dwnpart[1] / series.Count, 2);
            dwnpart[2] = (float)Math.Pow(dwnpart[2] / series.Count, 2);

            if(dwnpart[0] == 0 || dwnpart[1] == 0 || dwnpart[2] == 0)
                return new float[] { 0, 0, 0 };

            return new float[3] { uppart[0] / dwnpart[0] - 3, uppart[1] / dwnpart[1] - 3, uppart[2] / dwnpart[2] - 3 };
        }

        private static float Magnitude(List<float[]> series)
        {
            float[] avg = Mean(series);
            return (float)Math.Sqrt(Math.Pow(avg[0], 2) + Math.Pow(avg[1], 2) + Math.Pow(avg[2], 2));
        }

        private static float MagnitudeArea(List<float[]> series)
        {
            float marea = 0;

            for (int i = 0; i < series.Count; i++)
                marea += Math.Abs(series[i][0]) + Math.Abs(series[i][1]) + Math.Abs(series[i][2]);

            return marea / series.Count;
        }

        private static float AbsoluteDifference(List<float[]> series)
        {
            float magn = Magnitude(series);
            float diff = 0;

            for(int i = 0; i < series.Count; i++)
            {
                float si = (float)Math.Sqrt(Math.Pow(series[i][0], 2) + Math.Pow(series[i][1], 2) + Math.Pow(series[i][2], 2));
                diff += Math.Abs(si - magn);
            }

            return diff / series.Count;
        }

        private static float[] Quantels(List<float[]> series)
        {
            float[] x = new float[series.Count];
            float[] y = new float[series.Count];
            float[] z = new float[series.Count];

            for(int i = 0; i < series.Count; i++)
            {
                x[i] = series[i][0];
                y[i] = series[i][1];
                z[i] = series[i][2];
            }

            Array.Sort(x);
            Array.Sort(y);
            Array.Sort(z);

            // 0.10, 0.25, 0.50, 0.75, 0.90
            float[] quantels = new float[15];
            quantels[0] = x[(int)Math.Floor(0.1 * x.Length)];
            quantels[1] = x[(int)Math.Floor(0.25 * x.Length)];
            quantels[2] = x[(int)Math.Floor(0.5 * x.Length)];
            quantels[3] = x[(int)Math.Floor(0.75 * x.Length)];
            quantels[4] = x[(int)Math.Floor(0.9 * x.Length)];

            quantels[5] = y[(int)Math.Floor(0.1 * y.Length)];
            quantels[6] = y[(int)Math.Floor(0.25 * y.Length)];
            quantels[7] = y[(int)Math.Floor(0.5 * y.Length)];
            quantels[8] = y[(int)Math.Floor(0.75 * y.Length)];
            quantels[9] = y[(int)Math.Floor(0.9 * y.Length)];

            quantels[10] = z[(int)Math.Floor(0.1 * z.Length)];
            quantels[11] = z[(int)Math.Floor(0.25 * z.Length)];
            quantels[12] = z[(int)Math.Floor(0.5 * z.Length)];
            quantels[13] = z[(int)Math.Floor(0.75 * z.Length)];
            quantels[14] = z[(int)Math.Floor(0.9 * z.Length)];

            return quantels;
        }

        private static float[] �orrelation(List<float[]> series)
        {
            if (series.Count <= 1)
                return new float[] { 0, 0, 0 };

            float[] avg = Mean(series);
            float uppart = 0;
            float dwnpart = 0;
            float dwsuml = 0;
            float dwsumr = 0;
            float[] koefs = new float[3] { 0, 0, 0 };

            //correlation between X and Y
            for(int i = 0; i < series.Count; i++)
            {
                uppart += (series[i][0] - avg[0]) * (series[i][1] - avg[1]);
                dwsuml += (float)Math.Pow(series[i][0] - avg[0], 2);
                dwsumr += (float)Math.Pow(series[i][1] - avg[1], 2);
            }
            if ((float)Math.Sqrt(dwsuml * dwsumr) == 0)
                koefs[0] = 0;
            else
                koefs[0] = uppart / (float)Math.Sqrt(dwsuml * dwsumr); //XY

            //correlation between X and Z
            uppart = 0; dwsuml = 0; dwsumr = 0;
            for (int i = 0; i < series.Count; i++)
            {
                uppart += (series[i][0] - avg[0]) * (series[i][2] - avg[2]);
                dwsuml += (float)Math.Pow(series[i][0] - avg[0], 2);
                dwsumr += (float)Math.Pow(series[i][2] - avg[2], 2);
            }
            if ((float)Math.Sqrt(dwsuml * dwsumr) == 0)
                koefs[1] = 0;
            else
                koefs[1] = uppart / (float)Math.Sqrt(dwsuml * dwsumr); //XZ

            //correlation between Y and Z
            uppart = 0; dwsuml = 0; dwsumr = 0;
            for (int i = 0; i < series.Count; i++)
            {
                uppart += (series[i][1] - avg[1]) * (series[i][2] - avg[2]);
                dwsuml += (float)Math.Pow(series[i][1] - avg[1], 2);
                dwsumr += (float)Math.Pow(series[i][2] - avg[2], 2);
            }
            if ((float)Math.Sqrt(dwsuml * dwsumr) == 0)
                koefs[2] = 0;
            else
                koefs[2] = uppart / (float)Math.Sqrt(dwsuml * dwsumr); //YZ

            //[XY, XZ, YZ]
            return koefs;
        }
    }

    public class RoadObject : RoadInterface
    {
        public string name { get; set; }
        public float kk { get; set; }
        public int quality { get; set; }
        public string color { get; set; }
        public float icon_size { get; set; }
        public float guilt_probability
        {
            get
            {
                float pv = 0;
                float pq = 0;
                float pvw = 1;
                float pqw = 1.1f;
                float gps_speed_kph = this.gps_speed * 3.6f;
                
                if (gps_speed_kph <= 10)
                    pv = (float)(0.005 * gps_speed_kph);
                else if (gps_speed_kph <= 15)
                    pv = (float)(0.012 * gps_speed_kph - 0.07);
                else if (gps_speed_kph <= 25)
                    pv = (float)(0.024 * gps_speed_kph - 0.25);
                else if (gps_speed_kph <= 30)
                    pv = (float)(0.03 * gps_speed_kph - 0.4);
                else if (gps_speed_kph <= 35)
                    pv = (float)(0.026 * gps_speed_kph - 0.28);
                else if (gps_speed_kph <= 40)
                    pv = (float)(0.012 * gps_speed_kph + 0.21);
                else
                    pv = (float)(0.0051 * gps_speed_kph + 0.4833);

                if (this.quality <= 2)
                    pq = (float)(-0.1 * this.quality + 1.1);
                else if (this.quality <= 3)
                    pq = (float)(-0.4 * this.quality + 1.7);
                else if (this.quality <= 5)
                    pq = (float)(-0.25 * this.quality + 1.25);

                return ((pv * pvw) + (pq * pqw)) / (pvw + pqw);
            }
            set
            {

            }
        }

        public RoadObject() : base()
        {
            kk = 0;
            name = "unnamed_road_object";
            quality = 0;
            guilt_probability = 0;
        }
    }
}