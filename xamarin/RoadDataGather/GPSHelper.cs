using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RoadDataGather
{
    public class GPSHelper
    {
        public double Current_alt
        {
            get { lock (gps_locker) { return last_alt; } }
            set { }
        }

        public double Current_long
        {
            get { lock (gps_locker) { return last_long; } }
            set { }
        }

        public long Update_time
        {
            get { return time_diff; }
            set { }
        }

        public float Last_update_ArduinoGatherer_timestamp
        {
            get { return last_update_arduinogatherer_timestamp; }
            set { }
        }

        private double eQuatorialEarthRadius = 6378.1370D;
        private double d2r = (Math.PI / 180D);

        private System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        private long time_diff = 0;
        private float last_update_arduinogatherer_timestamp = 0;

        System.Object gps_locker = new System.Object();
        double last_long;
        double last_alt;
        double prev_long;
        double prev_alt;

        public GPSHelper()
        {
            stopwatch.Start();
        }

        public void Update(double new_long, double new_alt)
        {
            lock(gps_locker)
            {
                prev_alt = last_alt;
                prev_long = last_long;

                last_alt = new_alt;
                last_long = new_long;
                last_update_arduinogatherer_timestamp = ArduinoGatherer.Total_time;

                stopwatch.Stop();

                time_diff = stopwatch.ElapsedMilliseconds;

                stopwatch.Reset();
                stopwatch.Start();
            }
        }

        public double GetGpsSpeed()
        {
            double distance = 0;
            double speed = 0;

            lock (gps_locker)
            {
                distance = distanceInM(prev_alt, prev_long, last_alt, last_long);
                speed = distance == 0 ? 0 : distance / (time_diff/1000f);

                if (speed < 1)
                    speed = 0;
            }

            return speed;
        }

        public float[] GetPositionWithTimeCorrection(float timestamp)
        {
            var position = new float[2];

            if (timestamp - last_update_arduinogatherer_timestamp <= 0.01)
            {
                position[0] = (float)last_alt;
                position[1] = (float)last_long;
                return position;
            }

            var cur_speed = this.GetGpsSpeed();
            var trc = (float)Math.PI / 180f;
            // distance in meters
            var distance = (timestamp - last_update_arduinogatherer_timestamp) * cur_speed;

            // bearing angle in radians
            var y = Math.Sin(last_long * trc - prev_long * trc) * Math.Cos(last_alt * trc);
            var x = Math.Cos(prev_alt * trc) * Math.Sin(last_alt * trc) -
                    Math.Sin(prev_alt * trc) * Math.Cos(last_alt * trc) * Math.Cos(last_long * trc - prev_long * trc);
            var bearing = Math.Atan2(y, x);

            // next point
            var new_latitude = Math.Asin(Math.Sin(last_alt * trc) * Math.Cos(distance / 1000f / eQuatorialEarthRadius) +
                    Math.Cos(last_alt * trc) * Math.Sin(distance / 1000f / eQuatorialEarthRadius) * Math.Cos(bearing));
            var new_longitude = last_long * trc + Math.Atan2(Math.Sin(bearing) * Math.Sin(distance / 1000f / eQuatorialEarthRadius) * Math.Cos(last_alt * trc),
                                     Math.Cos(distance / 1000f / eQuatorialEarthRadius) - Math.Sin(last_alt * trc) * Math.Sin(new_latitude));

            position[0] = (float)new_latitude * 180f / (float)Math.PI;
            position[1] = (float)new_longitude * 180f / (float)Math.PI;
            return position;
        }

        private double distanceInM(double lat1, double long1, double lat2, double long2)
        {
            return (1000D * distanceInKM(lat1, long1, lat2, long2));
        }

        private double distanceInKM(double lat1, double long1, double lat2, double long2)
        {
            double dlong = (long2 - long1) * d2r;
            double dlat = (lat2 - lat1) * d2r;
            double a = Math.Pow(Math.Sin(dlat / 2D), 2D) + Math.Cos(lat1 * d2r) * Math.Cos(lat2 * d2r) * Math.Pow(Math.Sin(dlong / 2D), 2D);
            double c = 2D * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1D - a));
            double d = eQuatorialEarthRadius * c;

            return d;
        }
    }
}