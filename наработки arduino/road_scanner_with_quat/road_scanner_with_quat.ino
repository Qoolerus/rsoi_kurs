#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

MPU6050 mpu;

#define INTERRUPT_PIN 2

bool dmpReady = false;
uint8_t mpuIntStatus;
uint8_t devStatus;
uint16_t packetSize;
uint16_t fifoCount;
uint8_t fifoBuffer[64];

Quaternion q;
VectorInt16 aa;
VectorInt16 aaReal;
VectorInt16 aaWorld;
VectorInt16 gg;
VectorFloat gravity;

unsigned long timer = 0;
int delay_period = 0;
int inPacket = 0;

int calibrated = 1;
float coeffX = 1396;
float coeffY = -1396;
float coeffZ = -601;

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}

void calibrateAccelsByBox(int accuracy, const uint8_t* fifoBuffer) {
  if(abs(aaReal.x) > accuracy)
  {
    if(aaReal.x > 0)
      coeffX = coeffX - 0.08;
    else
      coeffX = coeffX + 0.08;

    mpu.setXAccelOffset(coeffX);
  }

  if(abs(aaReal.y) > accuracy)
  {
    if(aaReal.y > 0)
      coeffY = coeffY - 0.08;
    else
      coeffY = coeffY + 0.08;

    mpu.setYAccelOffset(coeffY);
  }

  if(abs(aaReal.z) > accuracy)
  {
    if(aaReal.z > 0)
      coeffZ = coeffZ - 0.08;
    else
      coeffZ = coeffZ + 0.08;

    mpu.setZAccelOffset(coeffZ);
  }

  Serial.print("iter: ");
  Serial.print(calibrated);
  Serial.print("\t");
  Serial.print(aaReal.x);
  Serial.print("\t");
  Serial.print(aaReal.y);
  Serial.print("\t");
  Serial.print(aaReal.z);
  Serial.print("\t");
  Serial.print(coeffX);
  Serial.print("\t");
  Serial.print(coeffY);
  Serial.print("\t");
  Serial.print(coeffZ);
  Serial.print("\n");
}

void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    Serial.begin(38400);
    while (!Serial);

    Serial.println(F("Initializing I2C devices..."));
    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();

    mpu.setFullScaleGyroRange(0);
    mpu.setFullScaleAccelRange(0);

    mpu.setXGyroOffset(39.8);
    mpu.setYGyroOffset(-45);
    mpu.setZGyroOffset(-1);

    mpu.setXAccelOffset(coeffX);
    mpu.setYAccelOffset(coeffY);
    mpu.setZAccelOffset(coeffZ);

    if (devStatus == 0) {
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
        Serial.print(digitalPinToInterrupt(INTERRUPT_PIN));
        Serial.println(F(")..."));
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }

    pinMode(3, OUTPUT);
    digitalWrite(3, HIGH);
}

void loop() {
    if (!dmpReady) return;
    
    while (!mpuInterrupt && fifoCount < packetSize) {
        if (mpuInterrupt && fifoCount < packetSize) {
          fifoCount = mpu.getFIFOCount();
        }  
    }

    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    fifoCount = mpu.getFIFOCount();

    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        mpu.resetFIFO();
        fifoCount = mpu.getFIFOCount();
        //Serial.println(F("FIFO overflow!"));
    } else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        fifoCount -= packetSize;

        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetAccel(&aa, fifoBuffer);
        mpu.dmpGetGyro(&gg, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetLinearAccel(&aaReal, &aa, &gravity);

        if(calibrated < 2800)
        {
          if(calibrated >= 800)
            calibrateAccelsByBox(1, fifoBuffer);
          calibrated++;
        }
        else
        {
          delay_period = (micros() - timer);
          timer = micros();

          Serial.print(aaReal.x); Serial.print("/"); Serial.print(aaReal.y); Serial.print("/"); Serial.print(aaReal.z); Serial.print("/");
          Serial.print(gg.x); Serial.print("/"); Serial.print(gg.y); Serial.print("/"); Serial.print(gg.z); Serial.print("/");
          //Serial.print(gravity.x); Serial.print("/"); Serial.print(gravity.y); Serial.print("/"); Serial.print(gravity.z); Serial.print("/");
          Serial.println(delay_period);
          ++inPacket;

          if(inPacket >= 25)
          {
            Serial.print('#');
            inPacket = 0;
          }
          else
            Serial.print('|');
        }
    }
}
